#SoREn

SoREn (Security Reconfigurable Engine) is a configurable platform
enforcing cyber-security policies. The platform defines a meta-model
for designing new components that can be dynamically installed during
SoREn execution.

##Acknowledgement

The work on SoREn was performed at [IFSTTAR](http://www.ifsttar.fr) in the framework of the [ELSAT2020](http://www.elsat2020.org) project which is co-financed by the European Union with the European Regional Development Fund, the French state and the Hauts de France Region Council.

##License

SoREn is published under the GPLv2 license (see LICENSE.txt)

It requires the Pymoult library, published under the GPLv2 license. 


##Folder Organisation

- ``platform`` : platform and components core code
- ``components`` : on-the-shelf components, ready to be installed
- ``sorenctl`` : graphical interface for reconfiguring SoREn
- ``configuration`` : example configuration files 

See the README.md files in each directory for installation and use
information
  
  




