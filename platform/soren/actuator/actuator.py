#    actuator.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import threading
import sys
from soren.platcore.componentupdate import InstallGenericComponentUpdate,UninstallGenericComponentUpdate
import datetime


class ActuatorInstallUpdate(InstallGenericComponentUpdate):
    """DSU Update class for installing an actuator

    :param actuator: actuator to be installed
    """
    def __init__(self,actuator):
        super().__init__(actuator)

    def check_alterability(self):
        #Always alterable for installing a component
        return True

    def apply(self):
        #Redefine probe.write to the write function of db
        self.component.lookup_marker = self.platform.configuration.lookup
        for r in self.component.responses:
            self.platform.response_handler.register_actuator(self.component,r)
        super().apply()

class ActuatorUninstallUpdate(UninstallGenericComponentUpdate):
    """DSU Update class for uninstalling an actuator

    :param aname: name of the actuator to be uninstalled
    """
    def __init__(self,actuator):
        super().__init__(actuator)

    def check_alterability(self):
        #Check if no situation requiring the component is present
        return True

    def apply(self):
        self.component.lookup_marker = None
        for r in self.component.responses:
            self.platform.response_handler.unregister_actuator(self.component,r)
        super().apply()


class CriticalSection(object):
    def __init__(self):
        self.lock = threading.Lock()
        self.inside = False
        self.cancel = False

    def reset(self):
        self.inside = False
        self.cancel = False

    def abort(self):
        self.lock.acquire()#Will block if already inside critical section
        r = False
        if not self.inside:
            self.cancel = True
            r = True
        self.lock.release()
        return r
        
    def __enter__(self):
        self.lock.acquire()
        if not self.cancel:
            self.inside = True
            return True
        else:
            return False

    def __exit__(self, e_typ, e_val, trcbak):
        self.inside = False
        self.lock.release()


class ActionThread(threading.Thread):
    def __init__(self,name,response):
        super().__init__()
        self.main = None
        self.name = name
        self.critical_section = CriticalSection()
        self.critical_section.reset()
        self.response = response

    def run(self):
        if self.main:
            self.main()
        if not self.response.is_completed():
            self.response.complete()

    def set_main(self,main):
        self.main = main

    def abort(self):
        return self.critical_section.abort()

    

        
class Actuator(object):
    """Base class for actuators

    :param name: name of the actuator
    :param response_stnames: sub-type names of each response the actuator can handle (list) 
    """
    def __init__(self,name,response_stnames):
        self.name = name
        self.responses = response_stnames
        self.ctype = "actuator"
        self.lastnumber = int(datetime.datetime.now().strftime("%Y%m%d%H%M%S")+"000000")


    def get_critical_section(self):
        """Returns the current thread critical section"""
        th = threading.current_thread()
        if hasattr(th,"critical_section"):
            return th.critical_section
        return None

    def get_response(self):
        """Returns the current thread response"""
        th = threading.current_thread()
        if hasattr(th,"response"):
            return th.response
        return None
        
    #Must be overriden by children classes
    def can_handle(self,response):
        """Returns True if the actuator can handle *response*
        
        This method **needs** to be extended by inheriting classes"""
        return False

    def lookup_marker(self,name,metatype):
        """Redefined to platform.configuration.lookup"""
    
    def get_part(self,pname):
        """Gets the named part from the system"""
        return self.lookup_marker(pname,"part")

    def spawn(self,response):
        """Called when a response of associated sub-type is created. Creates a
        thread and calls the ``run`` method in it

        :param response: response to handle
        """
        name = self.name+" handler "+str(self.lastnumber)
        self.lastnumber+=1
        thread = ActionThread(name,response)
        thread.set_main(self.main)
        response.handler = thread
        thread.start()
        return thread

    def install(self):
        """Creates an installation Update object"""
        return ActuatorInstallUpdate(self)
        
    def uninstall(self):
        """Creates an uninstallation Update object"""
        return ActuatorUninstallUpdate(self)
    
    #Must be overriden by new classes 
    def main(self):
        """Empty method. **Needs** to be extended by inherinting class.

        Called by ``run`` when spawning the actuator."""
        pass
