#    probe.py  This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import asyncio
from soren.platcore.componentupdate import InstallGenericComponentUpdate,UninstallGenericComponentUpdate
from soren.platcore.logger import log
import time

class ProbeInstallUpdate(InstallGenericComponentUpdate):
    """DSU Update class for installing a probe

    :param probe: probe to be installed"""
    def __init__(self,probe):
        super().__init__(probe)

    def check_alterability(self):
        #Always alterable for installing a component
        return True

    def apply(self):
        #Redefine probe.write to the write function of db
        self.component.write = self.platform.database.write_measure
        self.component.create_marker = self.platform.create_marker
        self.platform.probe_async_loop.add_coroutine(self.component.corloop())
        super().apply()

class ProbeUninstallUpdate(UninstallGenericComponentUpdate):
    """DSU Update class for uninstalling a probe

    :param pname: name of the probe to be uninstalled"""
    def __init__(self,probe):
        super().__init__(probe)

    def pre_alterability(self):
        self.component.stop()        

    def check_alterability(self):
        return self.component.is_stopped()

    def apply(self):
        self.component.write = None
        self.component.create_marker = None
        super().apply()


class Probe(object):
    """Base class for probes
    
    :param name: name of the probe
    """
    def __init__(self,name):
        self.name = name
        self.run = True
        self.stopped = False
        self.sleep_time = 0.5
        self.ctype = "probe"

    @asyncio.coroutine
    async def corloop(self):
        """Asynchronous Coroutine  

        calls the *main* method periodically and writes the measure it returns"""
        while self.run:
            await asyncio.sleep(self.sleep_time)
            try:
                m = self.main()
                if m:
                    self.write(m)
            except Exception as e:
                log("Probes","Probe "+self.name+" mainloop : "+str(e),1)
        self.stop_hook()
        self.stopped = True

    def set_sleep_time(self,time):
        """set sleep time between two calls to *main* in the coroutine

        :param time: a number of seconds (float)
        :return: None
        """
        self.sleep_time = time
            
    def stop(self):
        """Requests the probe to stop"""
        self.run = False

    def is_stopped(self):
        """Returns True if the probe is stopped"""
        return self.stopped

    #Will be defined by the platform when connecting the probe
    def write(self,measure):
        """Empty method, redefined to database.write when installing the probe"""
        pass

    def create_marker(self,metatype,type_name,**kwargs):
        """Empty method, redefined to platform.create_marker when installing the probe"""
        pass
    
    #Hook called when the probe is stopped
    #Can be overriden by children classes (not compulsory)
    def stop_hook(self):
        """Empty method, can be extended by inheriting classes.

        Called when the probe stops"""
        pass

    def create_measure(self,stname,**kwargs):
        return self.create_marker("measure",stname,**kwargs)

    def install(self):
        """Creates an installation Update object"""
        return ProbeInstallUpdate(self)
        
    def uninstall(self):
        """Creates an uninstallation Update object"""
        return ProbeUninstallUpdate(self)

    
    #Needs to be overridden by children classes
    def main(self):
        """Empty method. **Needs** to be extended by inherint class.

        Called peridocally in the corouting : program should be
        asynchronous if possible.

        :return: a measure instance
        """
        pass
    




















    







