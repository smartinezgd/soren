#    detector.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import asyncio
from datetime import datetime,timedelta
from soren.platcore.logger import log
from soren.platcore.componentupdate import InstallGenericComponentUpdate,UninstallGenericComponentUpdate
import time


class DetectorInstallUpdate(InstallGenericComponentUpdate):
    """DSU Update class for installing a detector

    :param detector: detector to be installed
    """
    def __init__(self,detector):
        super().__init__(detector)


    def check_alterability(self):
        #Always alterable for installing a component
        return True

    def apply(self):
        self.component.read_measure = self.platform.database.read_measure
        self.component.read_situation = self.platform.database.read_situation
        self.component.trigger_situation = self.platform.trigger_situation
        self.component.solve_situation = self.platform.solve_situation
        self.platform.detector_async_loop.add_coroutine(self.component.corloop())
        super().apply()

class DetectorUninstallUpdate(UninstallGenericComponentUpdate):
    """DSU Update class for uninstalling a detector

    :param dname: name of the detector to be uninstalled
    """
    def __init__(self,detector):
        super().__init__(detector)

    def pre_alterability(self):
        self.component.stop()        

    def check_alterability(self):
        return self.component.is_stopped()

    def apply(self):
        self.component.read_measure = None
        self.component.read_situation = None
        self.component.trigger_situation = None
        self.component.solve_situation = None
        super().apply()



class Detector(object):
    """Base class for detectors

    :param name: name of the detector
    :param delta: float (seconds) time delta of the context
    :param shift: float (seconds) time shift interval for each update default value is *delta/2*
    """
    def __init__(self,name,delta,shift=None):
        self.name = name
        self.ctype = "detector"
        self.run = True
        self.stopped = False
        self.sleep_time = 0.5
        self.bundle = []
        self.time_delta = timedelta(seconds=delta)
        self.begin_time = datetime.now()
        self.end_time = self.begin_time + self.time_delta
        if shift:
            self.time_shift = timedelta(seconds=shift)
        else:
            self.time_shift=(self.time_delta/2)

    def shift_time(self):
        """Shifts begin_time and end_time by time_shift"""
        if datetime.now() > self.end_time:
            self.begin_time+=self.time_shift
            self.end_time+=self.time_shift

    def update_wrapper(self):
        """Wrapper called by detectors calling update method"""
        self.bundle = self.update()
        self.shift_time()

    def get_measures(self,mstname,mfilter):
        """Wrapper function reading measures from database.
        
        Sets bounds with self.begin_time, self.end_time"""
        mfilter.update({"date":{"$gte":self.begin_time,"$lte":self.end_time}})
        return self.read_measure(mstname,mfilter)

    def read_measure(self,mstname,mfilter):
        """Empty method redefined to database.read_measure during installation"""
        pass
    
    @asyncio.coroutine
    async def corloop(self):
        """Asynchronous coroutine

        calls the *main* method periodically and creates (or solves) a
        situation depending on its return value
        """
        while self.run:
            await asyncio.sleep(self.sleep_time)
            try:
                self.update_wrapper()
                self.main()
            except Exception as e:
                log("Detectors","Detector "+self.name+" mainloop : "+str(e),1)
        self.stop_hook()
        self.stopped = True

    def set_sleep_time(self,time):
        """Sets sleep time between two calls to main in the coroutine

        :param time: a number of seconds (float)
        :return: None
        """
        self.sleep_time = time
            
    def stop(self):
        """Requests the detector to stop"""
        self.run = False

    def is_stopped(self):
        """Returns True if the detector is stopped"""
        return self.stopped

    def trigger_situation(self,sname,**kwargs):
        """Empty method redefined to platform.trigger_situation during installation"""
        pass

    def check_and_trigger_situation(self,sname,**kwargs):
        req = {"solve_date":"None"}
        req.update(kwargs)
        r = self.get_situations(sname,req)
        if len(r) == 0:
            return self.trigger_situation(sname,**kwargs)
        else:
            return None
    
    def solve_situation(self,situation):
        """Empty method redefined to platform.solve_situation during installation"""
        pass
    
    def read_situation(self,sname,sfilter):
        """Empty method redefined to database.read_situation during installation"""
        pass

    def get_situations(self,sname,sfilter={},unsolved=True):
        """Gets situations of given type, only unsolved by default"""
        if unsolved:
            sfilter.update({"solve_date":"None"})
        return self.read_situation(sname,sfilter)
    
    #Hook called when the detector is stopped
    #Can be overriden by children classes (not compulsory)
    def stop_hook(self):
        """Empty method, can be extended by inheriting class

        Called when the detector stops
        """
        pass

    def install(self):
        """Creates an installation Update object"""
        return DetectorInstallUpdate(self)
        
    def uninstall(self):
        """Creates an uninstallation Update object"""
        return DetectorUninstallUpdate(self)

        
    #Needs to be overridden by children classes
    def main(self):
        """Empty method. **Needs** to be extended by inherinting class.

        Called peridocally in the corouting : program should be asynchronous if possible.
        
        Returns: ``trigger`` if a situation must be created, ``reset``
        if the situation can be solved"""
        pass

    #Needs to be overridden by children classes
    def update(self):
        """Empty method. **Needs** to be extended by inheriting class.

        Called peridocally in the corouting : program should be asynchronous if possible.

        reads measures from database and returns a bundle of measures for analysis"""
        pass

    
















    







