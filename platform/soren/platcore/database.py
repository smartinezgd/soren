#    database.py  This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""Mongodb databe handling"""

from pymongo import MongoClient
from soren.platcore.measure import Measure
from soren.platcore.response import Response
from soren.platcore.situation import Situation
from soren.platcore.exceptions import DBException,InstanceExistsException
from soren.platcore.logger import log

class DatabaseConnector(object):
    """Database handler

    :param platform: Platform instance
    :param host: host of the mongodb server
    :param port: port of the mongodb server
    """

    instance=None
    def __init__(self,platform,host="localhost",port=27017):
        if DatabaseConnector.instance != None:
            raise InstanceExistsException("DatabaseConnector")
        self.host = host
        self.port = port
        self.client = None
        self.db = None
        self.platform = platform

    def connect(self):
        """Connects to the mongodb database"""
        self.client = MongoClient(self.host,self.port)
        self.db = self.client.opfplatform

    def check_collection(self,coll):
        return coll in self.db.collection_names()

    def write_measure(self,measure):
        """Writes a measure in the mongodb databse

        :param measure: measure to be written (instance of Measure)
        :return: None

        raises a DBException if measure of not instance of Measure
        """
        if isinstance(measure,Measure):
            measures = self.db.measures
            doc = measure.to_document()
            log("Database","Writing measure "+str(doc),3)
            measures.insert(doc)
        else:
            raise DBException("Error when writing measure : input is not of type Measure")
        
    def read_measure(self,mtype,mfilter):
        """Looks up a measure in the mongodb database

        :param mtype: Measure sub-type of the looked up measure
        :param mfilter: dictionnary containing values required on the looked up measure. 
        :return: None

        Calling with ``mtype="foo"`` and ``mfilter = {"key":value}`` will look up a measure of subtype *foo* which have attribute *key* with value *value*.  
        """
        if self.check_collection("measures"):
            measures = self.db.measures
            request = {"type":mtype}
            request.update(mfilter)
            r = measures.find(request)
            return list(filter(None.__ne__,[self.platform.build_marker_from_document(x) for x in r]))
        return []

        
    def write_situation(self,situation):
        if isinstance(situation,Situation):
            situations = self.db.situations
            doc = situation.to_document()
            log("Database","Writing situation "+str(doc),3)
            sid = situations.insert(doc)
            situation._id = sid
        else:
            raise DBException("Error when writing situation : input is not of type Situation")
        
    def read_situation(self,mtype,mfilter):
        if self.check_collection("situations"):   
            situations = self.db.situations
            request = {"type":mtype}
            request.update(mfilter)
            r = situations.find(request)
            return list(filter(None.__ne__,[self.platform.build_marker_from_document(x) for x in r]))
        return []


    def update_situation(self,situation,values):
        if self.check_collection("situations"):   
            situations = self.db.situations
            situations.update({"_id":situation._id},{"$set":values})
        
    def write_response(self,response):
        if isinstance(response,Response):
            responses = self.db.responses
            doc = response.to_document()
            log("Database","Writing response "+str(doc),3)
            rid = responses.insert(doc)
            response._id = rid
        else:
            raise DBException("Error when writing response : input is not of type Response")
        
    def read_response(self,mtype,mfilter):
        if self.check_collection("responses"):
            responses = self.db.responses
            request = {"type":mtype}
            request.update(mfilter)
            r = responses.find(request)
            return list(filter(None.__ne__,[self.platform.build_marker_from_document(x) for x in r]))
        return []

    def update_response(self,response,values):
        if self.check_collection("responses"):
            res = self.db.responses
            res.update({"_id":response._id},{"$set":values})




