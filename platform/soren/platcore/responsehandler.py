#    responsehandler.py  This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


from soren.platcore.logger import log
from soren.platcore.exceptions import InstanceExistsException
import datetime


class ResponseHandler(object):
    instance = None

    def __init__(self,platform):
        if ResponseHandler.instance != None:
            raise InstanceExistsException("ResponseHandler")
        ResponseHandler.instance = self
        self.registration = {}
        self.threads = {}
        self.platform = platform
        self.handled_responses = []

    def register_actuator(self,actuator,response_type_name):
        if response_type_name not in self.registration.keys():
            self.registration[response_type_name] = []
        self.registration[response_type_name].append(actuator)
        #Trigger actuator if unhandled response are still here
        resps = self.platform.database.read_response(response_type_name,{"completion_date":None})
        for r in resps:
            print(r)
            if not self.check_handled(r) and actuator.can_handle(r):
                th = actuator.spawn(r)
                self.threads[th.name] = th
                self.platform.database.update_response(r,{"handler":th.name})
                self.handled_responses.append(r)
        
    def unregister_actuator(self,actuator,response_type_name):
        if response_type_name in self.registration.keys():
            self.registration[response_type_name].remove(actuator)
            if len(self.registration[response_type_name]) == 0:
                del self.registration[response_type_name]
        
    def propagate_trigger(self,response):
        rtname = response.type_name
        handled = False
        if rtname in self.registration.keys():
            for activator in self.registration[rtname]:
                if activator.can_handle(response):
                    th = activator.spawn(response)
                    self.threads[th.name] = th
                    self.handled_responses.append(response)
                    self.platform.database.update_response(response,{"handler":th.name})
                    log("Response",str(response),2)
                    handled = True
                    break
        if not handled:
            log("Response","No installed actuator can handle "+str(response),1)  

    def get_thread(self,tname):
        if tname in self.threads.keys():
            return self.threads[tname]
        return None
            
    def check_handled(self,response):
        if not response.handler:
            return False
        else:
            return response.handler in self.threads.keys()

    def complete_response(self,response):
        self.platform.database.update_response(response,{"completion_date":response.completion_date,"handler":"None"})
        self.handled_responses.remove(response)
        log("Response",str(response),2)
        
    def lookup_handled_response(self,rtname,rfilter):
        #Check only equality in the filter
        #TODO : complete dbfilters filter 
        ans = []
        for r in self.handled_responses:
            if r.type_name == rtname:
                for key in rfilter.keys():
                    if key in r.__class__.parameters:
                        if r.get_parameter(key) == rfilter[key]:
                            ans.append(r)
                            break
                        elif hasattr(r,key):
                            if getattr(r,key) == rfilter[key]:
                                ans.append(r)
                                break
        return ans
                        
