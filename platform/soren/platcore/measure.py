#    measure.py  This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""Measure base class and manager"""
from datetime import datetime
from soren.platcore.logger import log

class Measure(object):
    """Base class for measures"""

    def get_field(self,field):
        return getattr(self,"field_"+field)
    
    def to_document(self):
        """create a document ready to be written as a mongodb document from
        the attributes with name of form field_*"""
        document = {}
        for field in self.__class__.fields:
            fvalue = getattr(self,"field_"+field)
            if isinstance(fvalue,Measure):
                document[field] = fvalue.to_document()
            else:
                document[field] = fvalue
        document["type"] = self.type_name
        document["metatype"] = "measure"
        return document

class MetaMeasure(type):
    def __new__(mc,name,bases,dic,fields):
        def init(inst,**kwargs):
            for field in fields:
                if field in kwargs.keys():
                    setattr(inst,"field_"+field,kwargs[field])
            inst.type_name = name
            if "_id" in kwargs.keys():
                inst._id = kwargs["_id"]
            else:
                inst._id = None
        dic.update({"__init__":init})
        return type.__new__(mc,name,bases+(Measure,),dic)

    def __init__(cls,name,bases,dic,fields):
        type.__init__(cls,name,bases,dic)
        cls.fields = fields
        cls.name = name
        if "date" not in cls.fields:
            cls.fields.append("date")
    
    def from_document(cls,document):
        kwargs = {}
        for field in cls.fields:
            if field in document.keys():
                kwargs[field] = document[field]
        kwargs["_id"] = document["_id"]
        return cls(**kwargs)        






