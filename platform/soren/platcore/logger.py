#    logger.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""Logging capabilities"""

import datetime
import threading
import os
from soren.platcore.exceptions import InstanceExistsException

class Logger(object):
    """Writes supplied messages to a configured logfile
    
    :param logfile: path to the logfile
    :param loglevel: log level between 0 and 3
    
    - At loglevel 0, nothing is logged.
    - At loglevel 1, only errors are logged.
    - At loglevel 2, only errors and key events are logged.
    - At loglevel 3, all operations of the platform are logged. 
    """
    
    instance = None
    def __init__(self,logfile,loglevel=1):
        if Logger.instance != None:
            raise InstanceExistsException("Logger")
        #Loglevel 0: nothing, 1: only errors, 2: errors and key events 3: everything
        self.loglevel = loglevel
        self.logfile = logfile
        self.lock = threading.Lock()
        Logger.instance = self

    def format_date(self,date):
        if type(date) == datetime.datetime:
            ms = round(float(date.microsecond)/1000000,3)
            return date.strftime("%Y-%m-%d:%H:%M:%S")+str(ms)[1:].ljust(4,"0")
        else:
            return ""
    def process_msg(self,message,*args):
        m = message
        for i in range(len(args)):
            if type(args[i]) == datetime.datetime:
                m = m.replace("%"+str(i),self.format_date(args[i]))
            else:
                m = m.replace("%"+str(i),args[i])
        return m
        
    def log(self,category,message,level,*args):
        """writes the message in the logfile if allowed by loglevel

        :param category: string indicating the category of the message
        :param message: message to be logged
        :param level: level of the message
        :return: None

        The message will be written only if its level is below the loglevel.

        This method is threadsafe.
        """

        if level <= self.loglevel:
            self.lock.acquire()
            now = datetime.datetime.now()
            string = self.format_date(now)+"  "+category+"  "+self.process_msg(message,*args)+"\n"
            f = open(self.logfile,"a")
            f.write(string)
            f.close()
            self.lock.release()

    def reconf(self,loglevel=None,logfile=None):
        self.lock.acquire()
        if loglevel:
            self.loglevel = loglevel
        if logfile and os.path.exists(os.path.dirname(logfile)):
            self.logfile = logfile
        self.lock.release()
        self.log("Logger","Reconfigured to verbosity "+str(self.loglevel)+" and logfile "+self.logfile,1)
            

def log(category,message,level,*args):
    """logging interface function. Calls ``log`` method of the Logger instance"""
    return Logger.instance.log(category,message,level,*args)
