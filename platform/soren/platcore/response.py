#    response.py  This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import soren.actuator.actuator as actuator
from soren.platcore.logger import log
from soren.platcore.responsehandler import ResponseHandler
import datetime


class Response(object):
    
    def complete(self):
        self.completion_date = datetime.datetime.now()
        self.handler = None
        ResponseHandler.instance.complete_response(self)

    def is_completed(self):
        return self.completion_date != None

    def is_handled(self):
        return (self.handler != None)

    def get_parameter(self,pname):
        return getattr(self,"param_"+pname)
    
    def abort(self):
        if self.handler:
            if not self.handler.abort():
                return False
        log("Response",str(self)+" aborted",2)
        self.complete()
        return True
    
    def __eq__(self,response):
        if response.type_name != self.type_name:
            return False
        elif abs(response.request_date-self.request_date) > datetime.timedelta(microseconds=1000):
            return False
        elif response.completion_date != None and self.completion_date and abs(response.completion_date-self.completion_date) > datetime.timedelta(microseconds=1000):
            return False
        elif response.__class__.parameters != self.__class__.parameters:
            return False
        elif response.handler != self.handler:
            return False
        else:
            for param in self.__class__.parameters:
                if self.get_parameter(param) != response.get_parameter(param):
                    return False
        return True
        
    def __hash__(self):
        return hash(self.__class__.__name__+str(self.trigger_date))

    def __str__(self):
        s = self.type_name+" requested at "+self.request_date.strftime("%Y-%m-%d:%H:%M:%S")
        if len(self.__class__.parameters) > 0:
            s+=" with parameters "
            s+=", ".join([param+"="+self.get_parameter(param) for param in self.__class__.parameters])
        if self.completion_date:
            s+=" completed at "+self.completion_date.strftime("%Y-%m-%d:%H:%M:%S")
        if self.is_handled():
            s+=" handled by "+self.handler.name
        return s
    
    def to_document(self):
        document = {}
        for param in self.__class__.parameters:
            value = getattr(self,"param_"+param)
            document[param] = value
        document["type"] = self.type_name
        document["metatype"] = "response"
        document["request_date"] = self.request_date
        if self.completion_date:
            document["completion_date"] = self.completion_date
        else:
            document["completion_date"] = "None"
        if self.handler:
            document["handler"] = self.handler.name
        else:
            document["handler"] = "None"
        if self._id:
            document["_id"] = self._id
        return document
    
class MetaResponse(type):
    def __new__(mc,name,bases,dic,parameters):
        #Each Response class created through this metaclass inherits from Response
        def init(inst,**kwargs):
            for param in parameters:
                if param in kwargs.keys():
                    setattr(inst,"param_"+param,kwargs[param])

            inst.type_name = name
            if "request_date" in kwargs.keys():
                inst.request_date = kwargs["request_date"]
            else:
                inst.request_date = datetime.datetime.now()
            if "completion_date" in kwargs.keys():
                inst.completion_date = kwargs["completion_date"]
            else:
                inst.completion_date = None
            if "handler" in kwargs.keys():
                inst.handler = ResponseHandler.instance.get_thread(kwargs["handler"])
            else:
                inst.handler = None
            if "_id" in kwargs.keys():
                inst._id = kwargs["_id"]
            else:
                inst._id = None
        dic.update({"__init__":init})
        return type.__new__(mc,name,bases+(Response,),dic)

    def __init__(cls,name,bases,dic,parameters):
        type.__init__(cls,name,bases,dic)
        #Class attribute containing registered actuators 
        cls.registered = []
        #Class attribute containing instances of responses
        cls.current = []
        cls.name = name
        cls.parameters = parameters

    def from_document(cls,document):
        kwargs = {}
        for param in cls.parameters:
            if param in document.keys():
                kwargs[param] = document[param]
        kwargs["_id"] = document["_id"]
        kwargs["request_date"] = document["request_date"]
        if document["completion_date"] != "None":
            kwargs["completion_date"] = document["completion_date"]
        if document["handler"] != "None":
            kwargs["handler"] = document["handler"]
        return cls(**kwargs)





