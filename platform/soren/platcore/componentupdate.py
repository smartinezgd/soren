#    componentupdate.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from pymoult.highlevel.updates import Update
from soren.platcore.logger import log


class InstallGenericComponentUpdate(Update):
    def __init__(self,component):
        super().__init__(component.name)
        self.component = component
        self.platform = None

    def set_platform(self,plat):
        self.platform = plat

    def check_alterability(self):
        return True
        
    def check_requirements(self):
        if not self.platform:
            return "never"
        comp = self.platform.configuration.lookup(self.component.name,self.component.ctype)
        if comp:
            log("DSU","A "+self.component.ctype+" named "+self.component.name+" is already installed, could not install new "+self.component.ctype,1)
            return "never"
        else:
            return "yes"

    def apply(self):
        self.platform.configuration.register(self.component,self.component.ctype)
        log("DSU",self.component.ctype.capitalize()+" "+self.component.name+" installed successfully",2)

        

class UninstallGenericComponentUpdate(Update):
    def __init__(self,component):
        super().__init__(component.name)
        self.component = component
        self.platform = None
        
    def set_platform(self,plat):
        self.platform = plat

    def check_alterability(self):
        return True
    
    def check_requirements(self):
        if not self.platform:
            return "never"
        return "yes"

    def apply(self):
        self.platform.configuration.unregister(self.component,self.component.ctype)
        log("DSU",self.component.ctype.capitalize()+" "+self.component.name+ " uninstalled successfully",2)
