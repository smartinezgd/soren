#    listener.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import pymoult.highlevel.listener
from pymoult.highlevel.listener import Listener
import re
import os
import imp
import threading
import sys
from soren.platcore.logger import log

parameters=re.compile(r"([^ =]+)=([^ =]+)")
configcmd=re.compile(r"pply ([^.]*\.yaml)")
scriptcmd=re.compile(r"pply ([^.]*\.py)(?: )?(.*)")
logreconf=re.compile(r"et (.*)") 

pymoult.highlevel.listener.Invoke_message = ""

class DSUListener(Listener):
    """DSU command receiver and handler

    :param platform: Platform instance
    :param name: name of the listener
    :param localhost: Indicates if the listener listens only on loopback interface
    :param port: Port on which commands are listened
    """
    def __init__(self,platform,name="DSU Listener",localhost=True,port=4242):
        super().__init__(name,localhost,port)
        self.platform = platform

    def parse_params(self,params):
        """Parses parameters given in installation commands

        :param params: the parameters string extracted from the command
        :return: a dictionnary {'parameter':value}
        """
        d = {}
        x = params.split(" ")
        for couple in x:
            mat = parameters.match(couple)
            if mat and len(mat.groups())==2:
                d[mat.group(1)] = mat.group(2)
        return d

    def start_update(self,message):
        """Parses received messages, extracts commands and launches updates

        :param message: received message
        :return: None 
        """
        #Independante log reconfiguration
        command = logreconf.match(message)
        if command:
            if len(command.groups())>=1:
                params = self.parse_params(command.group(1))
                conf = {}
                if "loglevel" in params.keys():
                    conf["loglevel"] = int(params["loglevel"])
                if "logfile" in params.keys():
                    conf["logfile"] = params["logfile"]
                #gen reconfiguration object
        command = configcmd.match(message)
        if command:
            if len(command.groups())>=1:
                conf = command.group(1)
                #gen update
                self.platform.configuration.parse_new_configuration(conf)
                updates = self.platform.configuration.gen_reconfiguration()
                for upd in updates:
                    upd.set_platform(self.platform)
                    self.platform.dsu_manager.add_update(upd)
        command = scriptcmd.match(message)
        if command: 
            if len(command.groups())>=1:
                module = command.group(1)
                params = {}
                if len(command.groups())>=2:
                    params.update(self.parse_params(command.group(2)))
                    upd = self.load_module(module,params)
        # if not upd or not isinstance(upd,reconf.Reconfiguration):
        #     log("DSU",'"'+message+'" does not produce a Reconfiguration, aborting',1)
        #     return False
        # else:
        #     self.platform.dsu_manager.add_update(upd)
        #     log("DSU","Reconfiguraton loaded",1)
        #     return True


    def load_module(self,modpath,params):
        if not os.path.exists(modpath):
            log("DSU","Could not install "+modpath+" : path does not exist",1)
            return None
        else:
            modfind = imp.find_module(modpath.rstrip(".py"))
            module = imp.load_module("update",modfind[0],modfind[1],modfind[2])
            try:
                upd = module.update(params)
                upd.set_platform(self.platform)
                return upd
            except Exception as e:
                log("DSU","An error was met when loading "+modpath+" :'"+str(e)+"'",1)
                return None

