#    This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""Configuration Management

Defines Scenarii 
Defines ConfigurationParser for handling platform reconfiguration
Defines Configuration for handling components, marker sub-types and scenarii
"""

from soren.platcore.exceptions import InstanceExistsException
from soren.platcore.configuration.parser import ConfigurationParser
from soren.platcore.configuration.elements import CfgParent
from soren.platcore.configuration.reconfiguration import LoggerReconfiguration
from soren.platcore.configuration.interp import ReconfigurationInterpretor
from soren.platcore.logger import log

class Configuration(object):
    """Configuration of the platform, registering scenarii, components etc """
    instance = None

    
    def __init__(self,platform):
        if Configuration.instance != None:
            raise InstanceExistsException("Configuration")
        self.parser = ConfigurationParser(platform)
        self.interpreter = ReconfigurationInterpretor(self)
        self.platform = platform
        self.current_configuration = CfgParent()
        self.target_configuration = None
        #Component management
        self.probes = {}
        self.detectors = {}
        self.deciders = {}
        self.actuators = {}
        #Marker type management
        self.measure_types = {}
        self.situation_types = {}
        self.response_types = {}
        #Parts
        self.parts = {}
        #syntax sugar
        self.meta_types_dicts = {"probe":self.probes,
                                 "detector":self.detectors,
                                 "decider":self.deciders,
                                 "actuator":self.actuators,
                                 "measure":self.measure_types,
                                 "situation":self.situation_types,
                                 "response":self.response_types,
                                 "part":self.parts}

    def register(self,element,metatype):
        if metatype in self.meta_types_dicts.keys():
            tdict = self.meta_types_dicts[metatype]
            s = metatype.capitalize()
            if element.name not in tdict.keys():
                tdict[element.name] = element
                log(s+"s",s+" "+element.name+" registered",3)
            else:
                log(s+"s",s+" "+element.name+" already exist. Could not register",1)

    def unregister(self,element,metatype):
        if metatype in self.meta_types_dicts.keys():
            tdict = self.meta_types_dicts[metatype]
            s = metatype.capitalize()
            if element.name in tdict.keys():
                del tdict[element.name]
                log(s+"s",s+" "+element.name+" unregistered",3)
            else:
                log(s+"s",s+" "+element.name+" was never registered. Could not unregister",1)

    def lookup(self,name,metatype):
        if metatype in self.meta_types_dicts.keys():
            tdict = self.meta_types_dicts[metatype]
            if name in tdict.keys():
                return tdict[name]
        return None

    def parse_new_configuration(self,conffile):
        self.target_configuration = self.parser.parse_config(conffile)

    def gen_reconfiguration(self):
        self.interpreter.parse(self.current_configuration,self.target_configuration)
        return self.interpreter.gen_updates()

    def add_scenario(self,scenario):
        #Append scenario with all its children (components, markers, ...)
        #Registeration of components and markers is done when installing them
        self.current_configuration.scenarii.append(scenario)

    def remove_scenario(self,scenario):
        #Remove scenario and all its children (components, markers, ...)
        #Unregistration of components and markers is done when uninstalling them
        self.current_configuration.scenarii.remove(scenario)
        
        
    
    
