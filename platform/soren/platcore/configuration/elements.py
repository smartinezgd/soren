#    configuration.elements.py is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""Internal Representation of elements of configuration

Specifies a configuration (current or to be installed)"""

import os
import imp
from soren.platcore.logger import log


class CfgParent(object):
    def __init__(self):
        self.logger = CfgLogger()
        self.scenarii = []


    def __eq__(self,parent):
        return self.logger == parent.logger and self.scenarii == parent.scenarii


class CfgLogger(object):
    def __init__(self,loglevel=2,logfile="soren.log"):
        self.loglevel = loglevel
        self.logpath = logfile

    def __eq__(self,cfglogger):
        return self.loglevel == cfglogger.loglevel and self.logpath == cfglogger.logpath

    def is_valid(self):
        if self.loglevel <= 3 and self.loglevel >= 0:
            try:
                f = open(self.logpath,"a")
                f.close()
            except IOError:
                return False
            return True
        return False
    

class CfgScenario(object):
    def __init__(self,name):
        self.name = name
        self.components = []
        self.parts = []
        self.marker_types = []
        self.links = []

    def add(self,element):
        if isinstance(element,CfgPart):
            self.parts.append(element)
        elif isinstance(element,CfgComponent):
            self.components.append(element)
        elif isinstance(element,CfgMarkerType):
            self.marker_types.append(element)

    def remove(self,element):
        if isinstance(element,CfgPart):
            self.parts.remove(element)
        elif isinstance(element,CfgComponent):
            self.components.remove(element)
        elif isinstance(element,CfgMarkerType):
            self.marker_types.remove(element)

    def get_component(self,name):
        for comp in self.components:
            if comp.name == name:
                return comp
        return None

    def get_marker_type(self,name):
        for mt in self.marker_types:
            if mt.name == name:
                return mt
        return None
            
    def get_situations(self):
        sits = []
        for mk in self.marker_types:
            if isinstance(mk,CfgSituation):
                sits.append(mk)
        return sits
            
    def implements_link(self,link):
        for l in self.links:
            if link < l:
                return True
        return False
            
    def __eq__(self,scenario):
        return self.name == scenario.name and\
            self.components == scenario.components and\
            self.parts == scenario.parts and\
            self.marker_types == scenario.marker_types and\
            self.links == scenario.links

    def __lt__(self,scenario):
        for link in self.links:
            if not scenario.implements_link(link):
                return False
        return True

    def __gt__(self,scenario):
        return scenario < self

    def __str__(self):
        return "Scenario "+self.name
                    

class CfgLink(object):
    def __init__(self,source,destination,markers):
        self.source = source
        self.destination = destination
        self.markers = markers

    def __eq__(self,link):
        return self.source == link.source and\
            self.destination == link.destination and\
            self.markers == link.markers

    def __lt__(self,link):
        mnames = [l.name for l in link.markers]
        for mk in self.markers:
            if mk.name not in mnames:
                return False
        return True

    def __gt__(self,link):
        return link < self

    def __str__(self):
        return "From "+self.source.name+" To "+self.destination.name+" with "+",".join([x.name for x in self.markers])
    
        
class CfgElement(object):
    def __init__(self,name):
        self.name = name

        
class CfgComponent(CfgElement):
    def __init__(self,name,classname,path,module,installation,parameters={}):
        super().__init__(name)
        self.path = path
        self.classname = classname
        self.parameters = parameters
        self.module = module
        self.installation = installation
        self.component = None

    def __eq__(self,comp):
        if self.path != comp.path:
            return False
        if self.name != comp.name:
            return False
        if self.classname != comp.classname:
            return False
        if self.parameters != comp.parameters:
            return False
        return True

    def get(self):
        return self.component

    def rename(self,name):
        self.name = name
        if self.component:
            self.get().rename(name)

    def modify(self,key,value):
        if self.parameters.has_key(key):
            self.parameters[key] = value
            comp = self.get()
            if self.comp:
                if hasattr(comp,"set_"+str(key)):
                    getattr(comp,"set_"+str(key))(value)
                else:
                    #comp has no setter for key, change it directly
                    setattr(comp,str(key),value)
        else:
            log("Components","Cannot change value of "+str(key)+" on "+self.name+". Component has no such parameter",1)

    def equivalent(self,comp):
        """Returns True if supplied component is equivalent to self.
        two components are equivalent if they have same path, classname, module and have same parameter keys"""
        if self.path != comp.path:
            return False
        if self.classname != comp.classname:
            return False
        if self.parameters.keys() != comp.parameters.keys():
            return False
        if self.module != comp.module:
            return False
        return True


    def load(self):
        if not self.component:
            if self.installation == "generic":
                modname = self.module.rstrip(".py")
                modpath = os.path.join(self.path,modname)
                if not os.path.exists(modpath+".py"):
                    log("Components","Could not load "+modpath+" : path does not exist",1)
                    return None
                modfind = imp.find_module(modpath)
                try:
                    mod = imp.load_module(modname,modfind[0],modfind[1],modfind[2])
                    self.component = getattr(mod,self.classname)(self.name,**self.parameters)
                except Exception as e:
                    log("Components","An error occured when loading "+modpath+" :'"+str(e)+"'",1)           
            else:
                modname = self.installation.rstrip(".py")
                modpath = os.path.join(self.path,modname)
                if not os.path.exists(modpath):
                    log("Components","Could not load "+modpath+" : path does not exist",1)
                    return None
                modfind = imp.find_module(modpath)
                try:
                    mod = imp.load_module(modname,modfind[0],modfind[1],modfind[2])
                    self.component = mod.update(self.parameters)
                except Exception as e:
                    log("Components","An error occured when loading "+modpath+" :'"+str(e)+"'",1)

class CfgProbe(CfgComponent):
    def __eq__(self,comp):
        if not type(comp) == CfgProbe:
            return False
        return super().__eq__(comp)

    def __str__(self):
        return "Probe "+self.name
    
class CfgDetector(CfgComponent):
    def __eq__(self,comp):
        if not type(comp) == CfgDetector:
            return False
        return super().__eq__(comp)

    def __str__(self):
        return "Detector "+self.name

    
class CfgDecider(CfgComponent):
    def __eq__(self,comp):
        if not type(comp) == CfgDecider:
            return False
        return super().__eq__(comp)

    def __str__(self):
        return "Decider "+self.name


class CfgActuator(CfgComponent):
    def __eq__(self,comp):
        if not type(comp) == CfgActuator:
            return False
        return super().__eq__(comp)

    def __str__(self):
        return "Actuator "+self.name

class CfgPart(CfgComponent):
    def __eq__(self,comp):
        if not type(comp) == CfgPart:
            return False
        return super().__eq__(comp)

    def __str__(self):
        return "Part "+self.name

class CfgMarkerType(CfgElement):
    def __init__(self,name,fields):
        super().__init__(name)
        self.fields = fields

    def __eq__(self,marker):
        if self.name != marker.name:
            return False
        if len(self.fields) != len(marker.fields):
            return False
        for f in self.fields:
            if f not in marker.fields:
                return False
        return True

class CfgMeasure(CfgMarkerType):
    def __eq__(self,marker):
        if not type(marker) == CfgMeasure:
            return False
        return super().__eq__(marker)

    def __str__(self):
        return "Measure type "+self.name+" "+",".join(self.fields)
    

class CfgSituation(CfgMarkerType):
    def __eq__(self,marker):
        if not type(marker) == CfgSituation:
            return False
        return super().__eq__(marker)

    def __str__(self):
        return "Situation type "+self.name+" "+",".join(self.fields)

    
class CfgResponse(CfgMarkerType):
    def __eq__(self,marker):
        if not type(marker) == CfgResponse:
            return False
        return super().__eq__(marker)

    def __str__(self):
        return "Response type "+self.name+" "+",".join(self.fields)

