#    reconfiguration.py  This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from soren.platcore.logger import log,Logger
from soren.platcore.configuration.elements import *
import datetime
import os
import yaml
import re




class ConfigurationParser(object):
    instance = None

    linkre = re.compile(r"(?P<src>[^ ]+) to (?P<dst>[^ ]+) with (?P<mks>(?:[^ ,]+, )*(?:[^ ,]+))$")
    
    def __init__(self,platform):
        if ConfigurationParser.instance != None:
            raise InstanceExistsException("ConfigurationParser")
        self.platform = platform
        ConfigurationParser.instance = self

    def parse_log(self,conf):
        cfg = CfgLogger(loglevel=Logger.instance.loglevel,logfile=Logger.instance.logfile)
        if "soren" in conf.keys():
            if "loglevel" in conf["soren"].keys():
                try:
                    loglevel = int(conf["soren"]["loglevel"])
                    cfg.loglevel = loglevel
                except ValueError:
                    pass
            if "logfile" in conf["soren"].keys():
                try:
                    logfile = str(conf["soren"]["logfile"])
                    cfg.logfile = logfile
                except ValueError:
                    pass
        return cfg

    
    def load_module(self,modpath,modname):
        if not os.path.exists(modpath):
            log("Platform","Could not load "+modpath+" : path does not exist",1)
            return None
        modfind = imp.find_module(modpath)
        module = imp.load_module(modname,modfind[0],modfind[1],modfind[2])
        return module


    def check_valid_spec(self,conf,name):
        if not "path" in conf.keys():
            log("Platform","Error when parsing specification of "+name+". No 'path' field specified",1)
            return False
        manifestpath = os.path.join(conf["path"],"MANIFEST.yaml")
        if not os.path.exists(manifestpath):
            log("Platform","Error : "+name+" from path "+path+" has no manifest file",1)
            return False
        with open(manifestpath,"r") as mftfile:
            manifest = yaml.safe_load(mftfile)
            for key in ["class","module","installation"]:
                if not key in manifest.keys():
                    log("Platform","Error : Manifest of "+name+" does not specify "+key,1)
                    return False
            if manifest["installation"] != "generic" and not manifest["installation"].endswith(".py"):
                log("Platform","Error : Unknown installation method in manifest of "+name,1)
                return False
        return True

    
    def parse_component(self,conf,cmpmetatype,compname):
        cmetatypes = {"probes":CfgProbe,"detectors":CfgDetector,
                      "deciders":CfgDecider,"actuators":CfgActuator}
        supplied = {"probes":CfgMeasure,"detectors":CfgSituation,
                      "deciders":CfgResponse}
        read = {"detectors":CfgMeasure,"deciders":CfgSituation,"actuators":CfgResponse}
        if not self.check_valid_spec(conf,compname):
            return (None,[])
        path = conf["path"]
        params = {}
        if "parameters" in conf.keys():
            for key in conf["parameters"].keys():
                params[key] = conf["parameters"][key]                        
        manifestpath = os.path.join(path,"MANIFEST.yaml")
        with open(manifestpath,"r") as mftfile:
            manifest = yaml.safe_load(mftfile)
            clsname = manifest["class"]
            module = manifest["module"]
            install = manifest["installation"]
            comp = cmetatypes[cmpmetatype](compname,clsname,path,module,install,params)
            markers = []
            if "supplies" in manifest.keys() and isinstance(manifest["supplies"],dict):
                for marker in manifest["supplies"]:
                    mk = supplied[cmpmetatype](marker,[])
                    if "fields" in manifest["supplies"][marker]:
                        for field in manifest["supplies"][marker]["fields"]:
                            mk.fields.append(field)
                    if mk not in markers:
                        markers.append(mk)
            if "reads" in manifest.keys() and isinstance(manifest["reads"],dict):
                for marker in manifest["reads"]:
                    mk = read[cmpmetatype](marker,[])
                    if "fields" in manifest["reads"][marker]:
                        for field in manifest["reads"][marker]["fields"]:
                            mk.fields.append(field)
                    if mk not in markers:
                        markers.append(mk)
        return (comp,markers)

    
    def parse_part(self,conf,name):
        if not self.check_valid_spec(conf,name):
            return None
        path = conf["path"]
        params = {}
        if "parameters" in conf.keys():
            for key in conf["parameters"].keys():
                params[key] = conf["parameters"][key]                        
        manifestpath = os.path.join(path,"MANIFEST.yaml")
        with open(manifestpath,"r") as mftfile:
            manifest = yaml.safe_load(mftfile)
            clsname = manifest["class"]
            module = manifest["module"]
            install = manifest["installation"]
            comp = CfgPart(name,clsname,path,module,install,params)
        return comp

    
    def parse_scenario(self,name,conf):
        """Parses a scenario"""
        scenario = CfgScenario(name)
        for comptype in ["probes","detectors","deciders","actuators"]:
            if comptype in conf.keys():
                for comp in conf[comptype].keys():
                    (cfgcomp,markers) = self.parse_component(conf[comptype][comp],comptype,comp)
                    if cfgcomp:
                        scenario.components.append(cfgcomp)
                        for m in markers:
                            if m not in scenario.marker_types:
                                scenario.marker_types.append(m)
                        
        if "parts" in conf.keys():
            for comp in conf["parts"].keys():
                cfgcomp = self.parse_part(conf["parts"][comp],comp)
                if cfgcomp:
                    scenario.parts.append(cfgcomp)
        #Links
        if "links" in conf.keys():
            for link in conf["links"]:
                m = ConfigurationParser.linkre.match(link)
                if m:
                    source = scenario.get_component(m.group("src"))
                    destination = scenario.get_component(m.group("dst"))
                    markers = [scenario.get_marker_type(x) for x in m.group("mks").split(", ")]
                    if source and destination and not None in markers:
                        l = CfgLink(source,destination,markers)
                        scenario.links.append(l)
        return scenario
                
        
    def parse_config(self,conffile):
        """Parses conffile and creates a reconfiguration object"""
        configuration = CfgParent()
        if not os.path.exists(conffile):
            log("Platform","Error : Configuration file "+str(conffile)+" not found",1)
            return None
        with open(conffile,"r") as cfgfile:
            conf = yaml.safe_load(cfgfile)
            configuration.logger = self.parse_log(conf)
            for scenario in conf.keys():
                if scenario != "soren":
                    sc = None
                    if isinstance(conf[scenario],str) and conf[scenario].endswith(".yaml"):
                        #scenario yaml file
                        if not os.path.exists(conf[scenario]):
                            log("Platform","Cannot load scenario "+scenario+" : file "+conf[scenario]+" not found",1)
                        else:
                            with open(conf[scenario],"r") as scfile:
                                scconf = yaml.safe_load(scfile)
                                sc = self.parse_scenario(scenario,scconf)
                    elif isinstance(conf[scenario],dict):
                        #scenario described
                        sc = self.parse_scenario(scenario,conf[scenario])
                    if sc:
                        configuration.scenarii.append(sc)

        return configuration
