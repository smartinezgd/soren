#    reconfiguration.py  This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from pymoult.highlevel.updates import Update
from soren.platcore.logger import log
from soren.platcore.measure import MetaMeasure
from soren.platcore.situation import MetaSituation
from soren.platcore.response import MetaResponse
import imp
import datetime
import time
import os
import yaml


class Reconfiguration(Update):
    """Reconfiguration class"""
    
    def __init__(self,functions=[],updates=[]):
        name = "Reconfiguration-"+datetime.datetime.now().strftime("%m:%d:%Y-%H:%M:%S")
        #No thread is associated with general reconfiguration
        #Specific reconfiguration should set the associated threads if needed
        super().__init__(name=name)
        self.updates = updates
        self.functions = functions
        self.platform = None
        self.situations = []
        #Time in seconds we accept to wait for alterability (aprox)
        self.alterability_time_limit = 3 


    def set_platform(self,plat):
        self.platform = plat
        for upd in self.updates:
            upd.set_platform(plat)

    def global_alterability(self):
        """Check global alterability : no situation can be present"""
        for s in self.situations:
            sits = self.platform.database.read_situation(s,{"solve_date":"None"})
            if len(sits)>0:
                return False
        return True
        
    def check_requirements(self):
        res = "yes"
        for upd in self.updates:
            req_upd = upd.check_requirements()
            if req_upd == "no":
                res = "no"
            elif req_upd == "never":
                return "never"
        return res

    def preupdate_setup(self):
        for upd in self.updates:
            upd.preupdate_setup()

    def wait_alterability(self):
        #Lock to-be obsolete situations
        for s in self.situations:
            self.platform.lock_situation(s)
        #Wait for global alterabilty
        not_glob_alt = True
        time_spent = 0
        while not_glob_alt:
            time.sleep(0.1)
            if self.global_alterability():
                not_glob_alt = False
            time_spent+=0.1
            if time_spent > self.alterability_time_limit:
                return False
        #Stop components
        for upd in self.updates:
            if hasattr(upd,"pre_alterability"):
                upd.pre_alterability()
        #Wait for components to be stopped
        clear = False
        while not clear:
            clear = True
            time.sleep(0.1)
            for upd in self.updates:
                if not upd.check_alterability():
                    clear = False
                    break
            time_spent+=0.1
            if time_spent > self.alterability_time_limit:
                return False
        #Alterability reached
        return True

    def clean_failed_alterability(self):
        #Unlock situations if alterability is canceled
        for s in self.situations:
            self.platform.unlock_situation(s)

    def preresume_setup(self):
        #Unlock situations after update
        for s in self.situations:
            self.platform.unlock_situation(s)

    def apply(self):
        for fun in self.functions:
            fun()
        for upd in self.updates:
            upd.apply()

    def wait_over(self):
        for upd in self.updates:
            upd.wait_over()


class LoggerReconfiguration(Update):
    def __init__(self,loglevel=None,logfile=None):
        super().__init__(name="Logger Reconfiguration")
        self.loglevel = loglevel
        self.logfile = logfile
        self.platform = None

    def set_platform(self,plat):
        self.platform = plat
        
    def apply(self):
        self.platform.logger.reconf(loglevel=self.loglevel,logfile=self.logfile)

    def check_alterability(self):
        return True

class InstallMarkerTypeUpdate(Update):
    def __init__(self,metatype,name,fields):
        super().__init__(metatype+" "+name+" installation")
        self.mtname = name
        self.metatype = metatype
        self.platform = None
        self.fields = fields

    def check_alterability(self):
        return True
        
    def set_platform(self,plat):
        self.platform = plat

    def apply(self):
        marker_type = None
        if self.metatype == "measure":
            marker_type = MetaMeasure(self.mtname,(),{},self.fields)
        elif self.metatype == "situation":
            marker_type = MetaSituation(self.mtname,(),{},self.fields)
        elif self.metatype == "response":
            marker_type = MetaResponse(self.mtname,(),{},self.fields)
        if marker_type:
            self.platform.configuration.register(marker_type,self.metatype)
            log("DSU",self.metatype.capitalize()+" "+self.mtname+" installed successfully",2)



class UninstallMarkerTypeUpdate(Update):
    def __init__(self,metatype,name):
        super().__init__(metatype+" "+name+" installation")
        self.mtname = name
        self.metatype = metatype
        self.platform = None

    def check_alterability(self):
        return True
        
    def set_platform(self,plat):
        self.platform = plat

    def apply(self):
        mt = self.platform.configuration.lookup(self.mtname,self.metatype)
        if mt:
            self.platform.configuration.unregister(mt,self.metatype)
            log("DSU",self.metatype.capitalize()+" "+self.mtname+" uninstalled successfully",2)
            



