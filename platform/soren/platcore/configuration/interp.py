#    configuration.interp.py is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""Interpretor for reconfiguration"""

from soren.platcore.exceptions import InstanceExistsException
from soren.platcore.configuration.elements import CfgComponent,CfgMeasure,CfgSituation,CfgResponse
from soren.platcore.configuration.reconfiguration import Reconfiguration,InstallMarkerTypeUpdate,UninstallMarkerTypeUpdate,LoggerReconfiguration
from soren.platcore.logger import log

class Block(object):
    """Block of instruction"""
    def __init__(self,sc_terms):
        self.terms = sc_terms

    def has_element(self,element):
        for term in self.terms:
            if term.has_element(element):
                return True
        return False

    def shares_element_with(self,arg):
        for term in self.terms:
            if term.shares_element_with(arg):
                return True
        return False

    def is_extension(self):
        if len(self.terms) < 2:
            return False
        #Only one addsc term is extension
        add_sc_term = None
        for sc_term in self.terms:
            if isinstance(sc_term,ScAdd):
                if add_sc_term == None:
                    add_sc_term = sc_term
                else:
                    return False
        #Scenarii of all non add_sc_term must be extended by scenario of add_sc_term
        add_sc = add_sc_term.target
        for sc_term in self.terms:
            if not sc_term is add_sc_term:
                sc = sc_term.target
                if not add_sc > sc:
                    return False
        return True

    #Prettyprinting
    def __str__(self):
        return "Block\n\t"+"\n\t".join([str(x) for x in self.terms])
        
    
class ScInstr(object):
    """Reconfiguration instruction for scenario"""
    def __init__(self,target,subterms):
        self.target = target
        self.subterms = subterms

    def has_element(self,element):
        for term in self.subterms:
            term_elem = term.target
            if type(element) == type(term_elem):
                if isinstance(term_elem,CfgComponent):
                    if term_elem.equivalent(element):
                        return True
                else:
                    if term_elem == element:
                        return True
        return False

    def shares_element_with(self,arg):
        for term in self.subterms:
            if arg.has_element(term.target):
                return True
        return False

    def __str__(self):
        return "\n\t\t".join([str(x) for x in self.subterms])
    

class ScRemove(ScInstr):
    """Remove target"""
    def __init__(self,target):
        subterms = []
        for part in target.parts:
            subterms.append(Remove(part))
        for mk in target.marker_types:
            subterms.append(Remove(mk))
        for comp in target.components:
            subterms.append(Remove(comp))
        super().__init__(target,subterms)

    def __str__(self):
        return "Remove Scenario "+self.target.name+"\n\t\t"+super().__str__() 

    
class ScAdd(ScInstr):
    """Add target"""
    def __init__(self,target):
        subterms = []
        for part in target.parts:
            subterms.append(Add(part))
        for mk in target.marker_types:
            subterms.append(Add(mk))
        for comp in target.components:
            subterms.append(Add(comp))
        super().__init__(target,subterms)

    def __str__(self):
        return "Add Scenario "+self.target.name+"\n\t\t"+super().__str__() 

class ElInstr(object):
    """Reconfiguration instruction for scenario element"""
    def __init__(self,target):
        self.target = target

        
class Add(ElInstr):
    """Add element"""

    def __str__(self):
        return "Add "+str(self.target)
    
class Remove(ElInstr):
    """Remove element"""

    def __str__(self):
        return "Remove "+str(self.target)


class Rename(ElInstr):
    """Rename component"""
    def __init__(self,target,new_name):
        self.new_name = new_name
        super().__init__(target)

    def __str__(self):
        return "Rename "+str(self.target)+" to "+self.new_name


class Modify(ElInstr):
    """Change a parameter of component"""
    def __init__(self,target,key,value):
        self.key = key
        self.value = value
        super().__init__(target)

    def __str__(self):
        return "Change parameter "+self.key+" to "+str(self.value)+" of "+str(self.target)


class Relink(ElInstr):
    """Relink element from sc1 to sc2"""
    def __init__(self,sc1,sc2,target1,target2):
        self.target1 = target1
        self.target2 = target2
        self.sc1 = sc1
        self.sc2 = sc2
        super().__init__(None)

    def __str__(self):
        return "Relink "+str(self.target1)+" from "+str(self.sc1)+" to "+str(self.sc2)+" ("+str(self.target2)+")"

class Link(ElInstr):
    """Link element from sc1 to sc2"""
    def __init__(self,sc,target,old_target):
        self.old_target = old_target
        self.sc = sc
        super().__init__(target)

    def __str__(self):
        return "Link "+str(self.target)+" to "+str(self.sc)+" ("+str(self.old_target)+")"





class ReconfigurationInterpretor(object):
    """Reconfiguration interpretor, generates reconfiguration objects"""
    instance = None
    
    def __init__(self,configuration):
        if ReconfigurationInterpretor.instance != None:
            raise InstanceExistsException("ReconfigurationInterpretor")
        self.current_terms = []
        self.configuration = configuration
        self.logger = None
    
    def parse(self,installed,target):
        self.current_terms = []
        for scenario in installed.scenarii:
            self.current_terms.append(ScRemove(scenario))
        for scenario in target.scenarii:
            self.current_terms.append(ScAdd(scenario))
        self.logger = target.logger

    def detect_and_create_one_block(self):
        new_term = None
        for term1 in self.current_terms:
            for term2 in self.current_terms:
                if term1 != term2 and term1.shares_element_with(term2):
                    if isinstance(term1,Block):
                        if isinstance(term2,Block):
                            #Merge blocks
                            new_term = Block(term1.terms)
                            for term in term2.terms:
                                new_term.terms.append(term)
                        elif isinstance(term2,ScInstr):
                            #Add term2 to block term1
                            new_term = term1
                            new_term.terms.append(term2)
                    elif isinstance(term1,ScInstr):
                        if isinstance(term2,Block):
                            #add term1 to block term2
                            new_term = term2
                            new_term.terms.append(term1)
                        elif isinstance(term2,ScInstr):
                            #create one block
                            new_term = Block([term1,term2])
                    self.current_terms.remove(term1)
                    self.current_terms.remove(term2)
                    self.current_terms.append(new_term)
                    return True
        return False



    def reduct_add_remove(self,rm_sc_term,rm_term,add_sc_term,add_term):
        rm_target = rm_term.target
        add_target = add_term.target
        if type(add_target) == type(rm_target):
            if isinstance(add_target,CfgComponent):
                if add_target == rm_target:
                    new_term = Relink(rm_sc_term.target,add_sc_term.target,rm_target,add_target)
                    #rm_sc_term.subterms.remove(rm_term)
                    #add_sc_term.subterms.remove(add_term)
                    return [new_term]
                elif add_target.equivalent(rm_target):
                    new_terms = []
                    new_terms.append(Relink(rm_sc_term.target,add_sc_term.target,rm_target,add_target))
                    #rm_sc_term.subterms.remove(rm_term)
                    #add_sc_term2.subterms.remove(add_term)
                    #modify
                    if rm_target.name != add_target.name:
                        new_terms.append(Rename(rm_target,add_target.name))
                    for key in rm_target.parameters.keys():
                        if add_target.parameters[key] != rm_target.parameters[key]:
                            new_terms.append(Modify(rm_target,key,add_target.parameters[key]))
                    return new_terms
            else:
                #If markers have the same name, they are considered equal for installation purpose
                if add_target.name == rm_target.name:
                    new_term = Relink(rm_sc_term.target,add_sc_term.target,rm_target,add_target)
                    #rm_sc_term.subterms.remove(rm_term)
                    #add_sc_term.subterms.remove(add_term)
                    return [new_term]
        return None

    def reduct_link(self,sc_term1,term1,sc_term2,term2):
        target1 = term1.target
        target2 = term2.target
        if type(target1) == type(target2):
            if isinstance(target1,CfgComponent):
                if target1 == target2:
                    new_term = Link(sc_term1.target,term2.target,term1.target)
                    return [new_term]
            else:
                if target1.name == target2.name:
                    new_term = Link(sc_term1.target,term2.target,term1.target)
                    return [new_term]
        return None
    
    
    #Reduct a block
    def reduct_block(self,block):
        modified = True
        while modified:
            modified = False
            for sc_term1 in block.terms:
                for sc_term2 in block.terms:
                    if id(sc_term1) != id(sc_term2):
                        new_sc1_subterms = [t for t in sc_term1.subterms]
                        new_sc2_subterms = [t for t in sc_term2.subterms]
                        for term1 in sc_term1.subterms:
                            for term2 in sc_term2.subterms:
                                if isinstance(term1,Remove) and isinstance(term2,Add):
                                    new_terms = self.reduct_add_remove(sc_term1,term1,sc_term2,term2)
                                    if new_terms:
                                        new_sc1_subterms.remove(term1)
                                        new_sc2_subterms.remove(term2)
                                        new_sc2_subterms+=new_terms
                                        modified = True
                                        break
                                elif isinstance(term1,Add) and isinstance(term2,Add):
                                    new_terms = self.reduct_link(sc_term1,term1,sc_term2,term2)
                                    if new_terms:
                                        new_sc1_subterms.remove(term1)
                                        new_sc1_subterms+=new_terms
                                        modified = True
                                        break
                                elif isinstance(term1,Add) and isinstance(term2,Relink):
                                    new_terms = self.reduct_link(sc_term1,term1,sc_term2,term2)
                                    if new_terms:
                                        new_sc1_subterms.remove(term1)
                                        new_sc1_subterms+=new_terms
                                        modified = True
                                        break
                            sc_term2.subterms = new_sc2_subterms
                        sc_term1.subterms = new_sc1_subterms
            

    def translate_term(self,term):
        if isinstance(term,Add):
            if isinstance(term.target,CfgComponent):
                term.target.load()
                if term.target.get():
                    return (None,term.target.get().install())
            else:
                if isinstance(term.target,CfgMeasure):
                    return (None,InstallMarkerTypeUpdate("measure",term.target.name,term.target.fields))
                elif isinstance(term.target,CfgSituation):
                    return (None,InstallMarkerTypeUpdate("situation",term.target.name,term.target.fields))
                elif isinstance(term.target,CfgResponse):
                    return (None,InstallMarkerTypeUpdate("response",term.target.name,term.target.fields))
        elif isinstance(term,Remove):
            if isinstance(term.target,CfgComponent):
                if term.target.get():
                    return (None,term.target.get().uninstall())
            else:
                if isinstance(term.target,CfgMeasure):
                    return (None,UninstallMarkerTypeUpdate("measure",term.target.name))
                elif isinstance(term.target,CfgSituation):
                    return (None,UninstallMarkerTypeUpdate("situation",term.target.name))
                elif isinstance(term.target,CfgResponse):
                    return (None,UninstallMarkerTypeUpdate("response",term.target.name))
        elif isinstance(term,Modify):
            def modify():
                term.target.modify(term.key,term.value)
            return (modify,None)
        elif isinstance(term,Rename):
            def rename():
                term.target.rename(term.new_name)
            return (rename,None)
        elif isinstance(term,Relink):
            def relink():
                #Registration to the configuration should be already done
                #Remove empty element target2 from sc2
                term.sc2.remove(term.target2)
                #Replace it with target1 from sc1
                term.sc2.add(term.target1)
                #Remove element target1 from sc1 
                term.sc1.remove(term.target1)
            return (relink,None)
        elif isinstance(term,Link):
            def link():
                #Registration to the configuration should be already done
                #Remove empty element target2 from sc2
                term.sc.remove(term.old_target)
                #Replace it with target1 from sc1
                term.sc.add(term.target)
            return (link,None)
        return (None,None)

    def translate_sc_instr(self,sc_instr):
        functions = []
        updates = []
        if isinstance(sc_instr,ScRemove):
            def remove_sc():
                self.configuration.remove_scenario(sc_instr.target)
            functions.append(remove_sc)
        elif isinstance(sc_instr,ScAdd):
            def add_sc():
                self.configuration.add_scenario(sc_instr.target)
            functions.append(add_sc)
        for term in sc_instr.subterms:
            (fun,upd) = self.translate_term(term)
            if fun:
                functions.append(fun)
            if upd:
                updates.append(upd)
        return (functions,updates)
                                            

    def translate_block(self,block):
        #Check if this block is an extension
        if block.is_extension():
            #block is an extension
            functions = []
            updates = []
            for sc_term in block.terms:
                (fun,upd) = self.translate_sc_instr(sc_term)
                functions+=fun
                updates+=upd
            update = Reconfiguration(functions=functions,updates=updates)
            #Extension only add, so no situations to lock for update
            update.situations = []
            return update
        else:
            functions = []
            updates = []
            sits = []
            for sc_term in block.terms:
                (fun,upd) = self.translate_sc_instr(sc_term)
                functions+=fun
                updates+=upd
                if isinstance(sc_term,ScRemove):
                    #We need to check situations for alterability if
                    #The scenario is removed
                    sits+=sc_term.target.get_situations()
            update = Reconfiguration(functions=functions,updates=updates)
            update.situations = [s.name for s in sits]
            return update
        
        
    def gen_updates(self):
        log("debug","Before interpretation",1)
        for term in self.current_terms:
            log("debug",str(term),1)
        
        while self.detect_and_create_one_block():
            #Keep creating blocks until none is detected
            pass
        new_terms = []
        for term in self.current_terms:
            if isinstance(term,Block):
                new_terms.append(term)
            elif isinstance(term,ScInstr):
                new_terms.append(Block([term]))
        self.current_terms = new_terms
        #All terms are blocks
        updates = []
        if self.logger:
            updates.append(Reconfiguration(updates=[LoggerReconfiguration(loglevel=self.logger.loglevel,logfile=self.logger.logfile)]))
        for block in self.current_terms:
            self.reduct_block(block)
            #Block is ready for creating updates
            update = self.translate_block(block)
            updates.append(update)
        log("debug","After interpretation",1)
        for term in self.current_terms:
            log("debug",str(term),1)
        return updates
    





