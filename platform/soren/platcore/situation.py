#    situation.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


"""Situation base class, meta-class and manager"""
import datetime
from soren.platcore.logger import log


class Situation(object):
    """Base class for situations""" 

    def solve(self):
        """solves the situation"""
        self.solve_date = datetime.datetime.now()

    def is_solved(self):
        """returns True if the situation is solved"""
        return self.solve_date != None

    def get_parameter(self,pname):
        return getattr(self,"param_"+pname)

    def __eq__(self,situation):
        if isinstance(situation,self.__class__):
            return (self.trigger_date == situation.trigger_date)
        else:
            return False

    def __hash__(self):
        return hash(self.__class__.__name__+str(self.trigger_date))

    def __str__(self):
        s = self.type_name+" triggered at "+self.trigger_date.strftime("%Y-%m-%d:%H:%M:%S")
        if len(self.__class__.parameters) > 0:
            s+=" with parameters "
            s+=", ".join([param+"="+self.get_parameter(param) for param in self.__class__.parameters])
        if self.solve_date:
            s+=" solved at "+self.solve_date.strftime("%Y-%m-%d:%H:%M:%S")
        return s
    
    def to_document(self):
        document = {}
        for param in self.__class__.parameters:
            value = getattr(self,"param_"+param)
            document[param] = value
        document["type"] = self.type_name
        document["metatype"] = "situation"
        document["trigger_date"] = self.trigger_date
        if self.solve_date:
            document["solve_date"] = self.solve_date
        else:
            document["solve_date"] = "None"
        if self._id:
            document["_id"] = self._id
        return document

class MetaSituation(type):
    """ """
    def __new__(mc,name,bases,dic,parameters):
        #Each Situation class created through this metaclass inherits from Situation
        def init(inst,**kwargs):
            for param in parameters:
                if param in kwargs.keys():
                    setattr(inst,"param_"+param,kwargs[param])
            inst.type_name = name
            if "trigger_date" in kwargs.keys():
                inst.trigger_date = kwargs["trigger_date"]
            else:
                inst.trigger_date = datetime.datetime.now()
            if "solve_date" in kwargs.keys():
                inst.solve_date = kwargs["solve_date"]
            else:
                inst.solve_date = None
            if "_id" in kwargs.keys():
                inst._id = kwargs["_id"]
            else:
                inst._id = None
        dic.update({"__init__":init})
        return type.__new__(mc,name,bases+(Situation,),dic)

    def __init__(cls,name,bases,dic,parameters):
        type.__init__(cls,name,bases,dic)
        #Class attribute containing instances of situations
        cls.current = []
        cls.name = name
        cls.parameters = parameters

    def from_document(cls,document):
        kwargs = {}
        for param in cls.parameters:
            if param in document.keys():
                kwargs[param] = document[param]
        kwargs["_id"] = document["_id"]
        kwargs["trigger_date"] = document["trigger_date"]
        if document["solve_date"] != "None":
            kwargs["solve_date"] = document["solve_date"]
        return cls(**kwargs)






