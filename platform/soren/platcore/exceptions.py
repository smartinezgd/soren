#    exceptions.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""Common exceptions"""

class InstanceExistsException(Exception):
    """Raised whenever an second instance of singleton is created
    
    :param singleton: the concerned singleton
    :param message: optional message
    """
    def __init__(self,singleton,message=None):
        self.singleton = singleton
        if message == None:
            message = "An instance of class "+str(singleton)+" already exists"
        super().__init__(message)

 
class DBException(Exception):
    """Exception raised whenever an issue concerning the databse is encountered
    
    :param message: message of the exception
    """
    def __init__(self,message):
        super().__init__(message)

        
