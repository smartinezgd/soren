#    opfplatform.py  This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import soren.platcore.componentmanager as compmanager
import soren.platcore.configuration as configuration
import soren.platcore.responsehandler as responsehandler
import soren.platcore.dsumanager as manager
import soren.platcore.listener as listener
import soren.platcore.logger as logger
import soren.platcore.database as db
import soren.platcore.measure as measure
import soren.platcore.situation as situation
import soren.platcore.response as response
import sys
import os
import yaml


class PlatformExistError(Exception):
    """Exception raised when trying to instanciate more than one Platform"""
    pass

    
class Platform(object):
    """Main class of the project. 
    Can be instanciated only once. 
    Subsequent instanciation will raise a PlatformExistError.

    :param logpath: path to the logfile
    :param loglevel: log level between 0 and 3  
    :param config: path to a configuration file

    **loglevel**

    loglevel indicates the verbosity of the platform. 
    It is an integer between 0 and 3.

    - At loglevel 0, nothing is logged.
    - At loglevel 1, only errors are logged.
    - At loglevel 2, only errors and key events are logged.
    - At loglevel 3, all operations of the platform are logged. 
    """

    instance = None
    def __init__(self,logpath,loglevel,config):
        if Platform.instance != None:
            raise PlatformExistError
        self.locked_situations = [] #Name of situation types locked for updates
        self.config = config
        self.configuration = configuration.Configuration(self)
        #Component Manager
        #self.component_manager = compmanager.ComponentManager()
        #Probe and Detector Async Loop
        self.probe_async_loop = compmanager.AsyncLoop("Probe Async Loop")
        self.detector_async_loop = compmanager.AsyncLoop("Detector Async Loop")
        self.decider_async_loop = compmanager.AsyncLoop("Decider Async Loop")
        #Response handler
        self.response_handler = responsehandler.ResponseHandler(self)
        #DSU Manager and listener
        self.dsu_manager = manager.DSUManager()
        self.dsu_listener = listener.DSUListener(self)
        #Logger
        self.logger = logger.Logger(logpath,loglevel=loglevel)
        #Database
        self.database = db.DatabaseConnector(self)
        #Pre-start setup
        Platform.instance = self

    def start(self):
        """Starts the platform, including all its components"""
        self.probe_async_loop.start()
        self.detector_async_loop.start()
        self.decider_async_loop.start()
        self.dsu_listener.start()
        self.dsu_manager.start()
        self.probe_async_loop.wait_start()
        self.detector_async_loop.wait_start()
        self.decider_async_loop.wait_start()
        self.database.connect()
        self.logger.log("Platform","SoREn plaform started",2)
        self.setup_from_config()

    def stop(self):
        """Stops the platform, including all its components"""
        self.probe_async_loop.stop()
        self.detector_async_loop.stop()
        self.logger.log("Platform","SoREn plaform stopped",2)

    def setup_from_config(self):
        """Setup configuration from a YaML file"""
        if self.config:
            self.configuration.parse_new_configuration(self.config)

    def create_marker(self,mt_metatype,mt_name,**kwargs):
        mt = self.configuration.lookup(mt_name,mt_metatype)
        if mt:
            return mt(**kwargs)

    def build_marker_from_document(self,document):
        mt = self.configuration.lookup(document["type"],document["metatype"])
        if mt:
            return mt.from_document(document)

    def trigger_situation(self,type_name,**parameters):
        if type_name not in self.locked_situations:
            s = self.create_marker("situation",type_name,**parameters)
            self.logger.log("Situation",str(s),2)
            self.database.write_situation(s)
            return s
        return None

    def solve_situation(self,situation):
        situation.solve()
        self.logger.log("Situation",str(situation),2)
        self.database.update_situation(situation,{"solve_date":situation.solve_date})

    def lock_situation(self,sname):
        if sname not in self.locked_situations:
            self.locked_situations.append(sname)

    def unlock_situation(self,sname):
        if sname in self.locked_situations:
            self.locked_situations.remove(sname)
        
    def request_response(self,type_name,**parameters):
        r = self.create_marker("response",type_name,**parameters)
        self.logger.log("Response",str(r),2)
        self.database.write_response(r)
        self.response_handler.propagate_trigger(r)
        return r

    def lookup_response(self,rtname,rfilter):
        responses1 = self.response_handler.lookup_handled_response(rtname,rfilter)
        responses2 = self.database.read_response(rtname,rfilter)
        if len(responses1) > 0:
            for r2 in responses2:
                if r2 not in responses1:
                    responses1.append(r2)
            return responses1
        else:
            return responses2
            
    def abort_response(self,rname,**rfilter):
        query = {"completion_date":"None"}
        query.update(rfilter)
        responses = self.lookup_response(rname,query)
        for response in responses:
            response.abort()



