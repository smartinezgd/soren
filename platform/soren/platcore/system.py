#    system.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""Internal representation of the watched system

The watched system is represented by an instance of *System* which
containts *parts* representing parts of the watched system on
which the platform can act"""
from soren.platcore.logger import log
from soren.platcore.exceptions import InstanceExistsException


class System(object):
    """Representation of the watched system
    
    :param platform: Platform instance
    """

    instance = None
    def __init__(self):
        if System.instance != None:
            raise InstanceExistsException("System")
        self.parts = {}
        System.instance = self
        
    def register(self,part):
        """Registers a part marker in the system

        :param part: a part instance
        :return: None
        """
        if part.name not in self.parts.keys():
            self.parts[part.name] = part
            log("System",part.name+" registered",2)
        else:
            log("System",part.name+" already used, could not register",1)

    def unregister(self,part):
        """Unregisters a part marker from the system

        :param part: a part instance
        :return: None
        """
        if part.name in self.parts.keys():
            del self.parts[part.name]
            log("System",part.name+" unregistered",2)
        else:
            self.platform.log("System",part.name+" does not exist, could not unregister",1)

    def lookup(self,pname):
        """Looks up the part marker with given name

        :param pname: name of the part
        :return: the part named *pname* or None
        """
        if pname in self.parts.keys():
            return self.parts[pname]
        else:
            return None


def lookup_part(pname):
    """Looks up named part marker in the system"""
    return System.instance.lookup(pname)

    
