#    part.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from soren.platcore.logger import log
from soren.platcore.componentupdate import InstallGenericComponentUpdate,UninstallGenericComponentUpdate

        
class Part(object):
    """Base class for part marker subtypes

    :param name: name of the part marker"""
    def __init__(self,name):
        self.name = name
        self.ctype = "part"

    def install(self):
        """Creates an installation Update object"""
        return InstallGenericComponentUpdate(self)
        
    def uninstall(self):
        """Creates an uninstallation Update object"""
        return UninstallGenericComponentUpdate(self)





