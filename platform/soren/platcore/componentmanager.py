#    componentmanager.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""Component manager and asynchronous loops"""

import asyncio
from threading import Thread,Event
from soren.platcore.logger import log

class AsyncLoop(Thread):
    """Generic asynchronous loop container. Hosts a loop in its own thread.
    
    :param name: name of the loop"""
    def __init__(self,name):
        self.loop = None
        self.event = Event()
        self.event.clear()
        super().__init__(name=name)
        
    def run(self):
        """Starts the asynchronous loop"""
        self.loop = asyncio.get_event_loop_policy().new_event_loop()
        self.event.set()
        self.loop.run_forever()

    def stop(self):
        """Stops asynchronous loop"""
        self.loop.stop()

    def add_coroutine(self,coroutine):
        """Adds a coroutine to the asynchronous loop"""
        asyncio.run_coroutine_threadsafe(coroutine,self.loop)

    def wait_start(self):
        """Returns when the asynchronous loop is started"""
        self.event.wait()

