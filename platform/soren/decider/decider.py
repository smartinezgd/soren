#    decider.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import asyncio
import time
from soren.platcore.logger import log
from soren.platcore.componentupdate import InstallGenericComponentUpdate,UninstallGenericComponentUpdate
import traceback

class DeciderInstallUpdate(InstallGenericComponentUpdate):
    """DSU Update class for installing a decider

    :param decider: decider to be installed
    """
    def __init__(self,decider):
        super().__init__(decider)

    def check_alterability(self):
        #Always alterable for installing a component
        return True

    def apply(self):
        self.component.get_situations = self.platform.database.read_situation
        self.component.get_responses = self.platform.lookup_response
        self.component.lookup_marker = self.platform.configuration.lookup
        self.component.request_response = self.platform.request_response
        self.component.abort_response = self.platform.abort_response
        self.platform.decider_async_loop.add_coroutine(self.component.corloop())
        super().apply()

class DeciderUninstallUpdate(UninstallGenericComponentUpdate):
    """DSU Update class for uninstalling a decider

    :param dname: name of the decider to be uninstalled
    """
    def __init__(self,decider):
        super().__init__(decider)

    def pre_alterability(self):
        self.component.stop()        

    def check_alterability(self):
        return self.component.is_stopped()

    def apply(self):
        self.component.get_situations = None
        self.component.get_responses = None
        self.component.lookup_marker = None
        self.component.request_response = None
        self.component.abort_response = None
        super().apply()

class Decider(object):
    """Base class for deciders

    :param name: name of the decider
    :param situation_subtypes: sub-types of associated situations
    :param response_subtypes: sub-types of associated responses
    """
    def __init__(self,name,situation_subtypes,response_subtypes):
        self.name = name
        self.run = True
        self.ctype = "decider"
        self.stopped = False
        self.sleep_time = 0.5
        self.situation_subtypes = situation_subtypes
        self.response_subtypes = response_subtypes
        self.bundle = {"situations":[],"responses":[]}
        
    @asyncio.coroutine
    async def corloop(self):
        """Asynchronous coroutine
        calls the *main* method periodically"""
        while self.run:
            await asyncio.sleep(self.sleep_time)
            try:
                self.update_bundle()
                self.main()
            except Exception as e:
                log("Deciders","Decider "+self.name+" mainloop : "+str(e),1)
        self.stop_hook()
        self.stopped = True

    def get_situations(self,stype,sfilter):
        """Redefined to database.read_situation at install"""
        pass

    def get_responses(self,rtype,rfilter):
        """Redefined to platform.lookup_response at install"""

    def update_bundle(self):
        self.bundle["situations"] = []
        self.bundle["responses"] = []
        for stname in self.situation_subtypes:
            s = self.get_situations(stname,{"solve_date":"None"})
            self.bundle["situations"]+=list(filter(None.__ne__,s))
        for rtname in self.response_subtypes:
            r = self.get_responses(rtname,{"completion_date":"None"})
            self.bundle["responses"]+=list(filter(None.__ne__,r))
        
    def set_sleep_time(self,time):
        """Sets sleep time between two calls to main in the coroutine

        :param time: a number of seconds (float)
        :return: None
        """
        self.sleep_time = time

    def stop(self):
        """Requests the decider to stop"""
        self.run = False

    def is_stopped(self):
        """Returns True if the detector is stopped"""
        return self.stopped

    #Hook called when the detector is stopped
    #Can be overriden by children classes (not compulsory)
    def stop_hook(self):
        """Empty method, can be extended by inheriting class

        Called when the decider stops
        """
        pass

    def check_and_request_response(self,rname,**kwargs):
        req = {"completion_date":"None"}
        req.update(kwargs)
        r = self.get_responses(rname,req)
        if len(r) == 0:
            return self.request_response(rname,**kwargs)
        else:
            return None

    def abort_response(self,rname,rfilter):
        """Redefined to platform.abort_response on install"""
        pass
    
    def request_response(self,rname,**kwargs):
        """Redefined to platform.request_response on install"""
        pass

    def lookup_marker(self,name,metatype):
        """Redefined to platform.configuration.lookup"""
    
    def get_part(self,pname):
        """Gets the named part from the system"""
        return self.lookup_marker(pname,"part")
    
    def install(self):
        """Creates an installation Update object"""
        return DeciderInstallUpdate(self)
        
    def uninstall(self):
        """Creates an uninstallation Update object"""
        return DeciderUninstallUpdate(self)
    
    #Needs to be overridden by children classes
    def main(self):
        """Empty method. **Needs** to be extended by inherinting class.

        Called peridocally in the corouting : program should be asynchronous if possible.
        
        should call ``request_response`` to create a response"""
        pass
