#SoREn platform

The central platform of the Security Reconfigurable Engine (SoREn)

##Requirements

- **Python3-yaml** for parsing and writing configuration files
- **Python3-pymongo** for interfacing with the database
- **MongoDB** the database
- **Python-dsu3** DSU enhanced Python interpreter
- **Pymoult** DSU library

Python-dsu3 and Pymoult can be found on bitbucket
__https://bitbucket.org/smartinezgd/pymoult__

##Installation

Run `python3 setup.py install`

You may specify a __--prefix__ option with a path in your PYTHONPATH

##Running

run `soren`. A folder will be created in your home directory to
contain the database and log files.

The `soren` command accepts parameters, run `soren help` to see documentation


  
  




