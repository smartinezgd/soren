#!/usr/bin/env python3

from distutils.core import setup

setup(name="soren",
      version="1.0",
      description="Security Reconfigurable Engine (SoREn)",
      author="Sébastien Martinez",
      author_email="sebastien.martinez@ifsttar.fr",
      scripts=["bin/soren","bin/sorenplatform.py"],
      packages=["soren","soren.platcore","soren.probe","soren.detector","soren.decider","soren.actuator","soren.platcore.configuration"],
)
      
