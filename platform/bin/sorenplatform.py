#!/usr/bin/env python-dsu3
# -*- coding: utf-8 -*-

#    sorenplatform.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


"""Entry point of SoREn 

``./platform.py [PARAM1] [ARG1] ...``

- ``--config``, ``-c``  [file]     : loads configuration from file 
- ``--log``, ``-l``     [file]     : sets the log file (default : opf.log)
- ``--verbose``, ``-v`` [loglevel] : sets loglevel (default : 1)

"""

from soren.platcore.opfplatform import Platform
import signal
import argparse
import sys
import os


def sigint_handler_generator(platform):
    """Generates a handler called when SIGINT signal is received
    :param platform: platform instance
    :return: a signal handler function
    """
    def handler(signal,frame):
        platform.stop()
        try:
            sys.exit(0)
        except SystemExit:
            print("Exiting Platform")
            os._exit(0)
    return handler


class VerboseAction(argparse.Action):
    """Action for parsing the -vvvv command-line parameter"""
    def __call__(self,parser, args, values, option_string=None):
        if values==None:
            #Happens if -v was given as paremeter
            values=1
        else:
            try:
                values=int(values)
            except ValueError:
                values = values.count('v')+1
        setattr(args, self.dest, values)


if __name__ == "__main__":
    #Init arg parser
    parser = argparse.ArgumentParser(description='OPF Platform executable')
    parser.add_argument("-c",'--config',type=str,help='path to a configuration file', default="",dest="config")
    parser.add_argument("-l",'--log',type=str,help='path to logfile', default="opf.log",dest="logfile")
    parser.add_argument("-v",'--verbose',nargs='?',help='verbose level',action=VerboseAction,default=0,dest="verbose")
    args = parser.parse_args()
    platform = Platform(args.logfile,args.verbose,args.config)
    signal.signal(signal.SIGINT, sigint_handler_generator(platform))
    platform.start()
