#    dprobe.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import soren.probe.probe as probe
import re
from datetime import datetime

class DeauthProbe(probe.Probe):
    """Probe for the deauthentication scenario

    Reads a frame from a trace file each 0.1ms"""
    def __init__(self,name,trace=None):
        self.trace = trace
        self.open_trace()
        self.regex = re.compile(r'"[0-9]+","([0-9]\.[0-9]+)","[^"]*","[^"]*","[^"]*","[^"]*","([^"]*)"')
        super().__init__(name)
        self.sleep_time = 0.0001
        self.time = 0
        self.lastline = None

    def open_trace(self):
        """opens the trace file"""
        #open trace
        self.traceptr = open(self.trace,"r")
        
    def stop_hook(self):
        """closes the trace file"""
        #close trace
        self.traceptr.close()
    
    def main(self):
        """Reads one frame from the trace file

        Creates a DeauthMeasure frame if the frame is a deauthentication frame"""
        #Read one trace line
        line = ""
        if self.lastline:
            line = self.lastline
        else:
            line = self.traceptr.readline()
        if line == "":
            self.traceptr.close()
            self.traceptr = open(self.trace,"r")
            self.time = 0
        else:
            time = float(line.split('","')[1])
            if time <= self.time:
                self.lastline = ""
                mat = self.regex.match(line.strip())
                if mat:
                    date = datetime.now()
                    analysis = mat.group(2)
                    if "Deauthentication" in analysis:
                        return self.create_measure("DeauthMeasure",date=date)
            else:
                self.lastline = line
        self.time+=self.sleep_time

