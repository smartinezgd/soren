#    update.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import sys
import os
sys.path.insert(0,os.path.dirname(__file__))
import dprobe

def update(params):
    """Standard function called by the platform core listener when installing the component. Takes a dictionary containing parameters sent to the component constructor. 

    Required dictionary keys and values :
    
    - *name*  : name of the component (string)
    - *trace* : path to a trace containing Wi-Fi frames"""
    return dprobe.DeauthProbe(params["name"],params["trace"]).install()
