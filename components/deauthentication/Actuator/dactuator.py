#    dactuator.py  This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import soren.actuator.actuator as actuator
import time


class DeauthActuator(actuator.Actuator):
    """Actuator for deauthentication scenario

    Switches Channel marker state to Wi-Fi or LTE"""
    def __init__(self,name):
        super().__init__(name,["ConSwitchResponse"])
        
    def can_handle(self,response):
        """Returns True as the actuator can handle any ConSwitchResponse"""
        return True
    
    def main(self):
        """Switches channel marker state"""
        time.sleep(1)
        with self.get_critical_section() as cs:
            if cs:
                if self.get_response().get_parameter("target") == "Wifi":
                    self.get_part("protocol").switch_wifi()
                elif self.get_response().get_parameter("target") == "4G":
                    self.get_part("protocol").switch_4g()

