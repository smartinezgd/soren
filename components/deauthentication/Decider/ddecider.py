#    ddecider.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import soren.decider.decider as decider
from soren.platcore.logger import log
import time


class DeauthDecider(decider.Decider):
    """Decider for the deauthentication scenario

    Periodically scans situations looking for DeauthSituation
    markers. Triggers the creation of a ConSwitchResponse marker
    whenever the Protocol marker is set on Wi-Fi and a DeauthSituation
    is noticed.
    """
    def __init__(self,name):
        super().__init__(name,["DeauthSituation"],["ConSwitchResponse"])
        self.sleep_time = 0.2

    def main(self):
        """Intelligence of the component. Scans the situations, reads the
        state of the Protocol part, triggers the creation of responses.
        """
        channel = self.get_part("protocol").nature
        if len(self.bundle["situations"]) == 0 and channel == "4G":
            self.check_and_request_response("ConSwitchResponse",target="Wifi")
        for s in self.bundle["situations"]:
            if s.type_name == "DeauthSituation" and channel == "Wifi":
                self.check_and_request_response("ConSwitchResponse",target="4G")





                

