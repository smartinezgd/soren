#    ddetector.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import soren.detector.detector as detector

        
class DeauthDetector(detector.Detector):
    """Detector for the deauthentication scenario
    
    Scans measures each 100ms and creates a DeauthSituation when 5
    DeauthMeasure are received within 200ms."""
    def __init__(self,name):
        super().__init__(name,0.2)
        self.sleep_time = 0.1
        self.trigger_floor = 5
        self.reset_floor = 2
        
    def update(self):
        """Reads DeauthMeasure markers from the database"""
        return self.get_measures("DeauthMeasure",{})
        
    def main(self):
        """Intelligence of the component

        Counts measures and triggers creation of DeauthSituation"""
        count = len(self.bundle)
        if count >= self.trigger_floor:
            self.check_and_trigger_situation("DeauthSituation")
        elif count <= self.reset_floor:
            situations = self.get_situations("DeauthSituation")
            for s in situations:
                self.solve_situation(s)
    
