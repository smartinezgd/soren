#    dpart.py  This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import soren.platcore.part as part
from soren.platcore.logger import log

class Protocol(part.Part):
    """Part for the deauthentication scenario

    Two states : Wi-Fi and LTE
    """
    def __init__(self,name):
        self.nature = "Wifi"
        self.frequency = "850"
        super().__init__(name)

    def switch_wifi(self):
        """Switches state to Wi-Fi if curent state is LTE"""
        if self.nature == "4G":
            self.nature = "Wifi"
            log("System","Protocol switched to Wifi",2)

    def switch_4g(self):
        """Switches state to LTE if curent state is Wi-Fi"""
        if self.nature == "Wifi":
            self.nature = "4G"
            log("System","Protocol switched to 4G",2)

