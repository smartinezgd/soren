#Example components

On-The-Shelf components, ready to be used. For each scenario,
components are organized as folders containing source files and data
files (in the case of probes)

##Scenario

Components for two scenario are provided

* **Deauthentication** Enforces protocol switching when deauthentication attack are detected
  - *Probe* Reads captured Wi-Fi frames looking for deauthentication frames
  - *Detector* Detect deauthentication attacks reading deauthentication frames
  - *Decider* Triggers protocol switching when deauthentication attack is detected when using Wi-Fi
  - *Acuator* Switches protocol between Wi-Fi and 4G 
  - *Protocol* Part marker representing protocol in use (Wi-Fi or 4G)
* **Jamming** 
  - *Probe* Reads EVM values from a file and creates EVM measure markers
  - *Detector* Detects 4G jamming reading EVM measures
  - *Decider* Triggers protocol switching and/or application layer protection
  - *Acuator* Switches application layer protection 
  - *AppLayer* Par marker representing strength of application layer protection (economic or reinforced)
  
##Developping components

Components must be organised as folder containing their source
files. Each folder must contain a `update.py` Python module that
defines an `update(params)` method where `params` is a dictionnary of
arguments. The method must return an instance of the component. An
instance of component is an instance of any class inheriting from a
base component class (*Probe*,*Detector*,*Decider*,*Protocol*).

Other modules can be organised as you wish but keep in mind that in
Python, module references are bound to their names : if a module of a
given name is already imported, any other module of the same name will
not be imported. As a consequence, component modules should have
unique names. 

Part markers are treated as components when installing or
uninstalling, therefore when developping a new part marker, you should
follow the same rules (except Part marker classes inherit from *Part*
class).



