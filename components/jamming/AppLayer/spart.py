#    spart.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

import soren.platcore.part as part
from soren.platcore.logger import log

class AppLayer(part.Part):
    """Part for the jamming scenario

    Has two states : *economic* and *reinforced*"""
    def __init__(self,name):
        self.strength = "economic"
        super().__init__(name)

    def reinforce(self):
        """Switches to *strong* if current state is *economic*"""
        if self.strength == "economic":
            self.strength = "strong"
            log("System","Application layer protocols reinforced",2)

    def weaken(self):
        """Switches to *economic* if current state is *strong*"""
        if self.strength == "strong":
            self.strength = "economic"
            log("System","Application layer protocols on economic status",2)

