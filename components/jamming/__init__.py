#    This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

"""Components and part marker handling jamming attack

This scenario is designed as a complement to the deauthentication
scenario. 

*AppLayer* is the part marker to be installed in the platform,
 representing application layer protection (authentication,
 encryption) protols used by the watched system.

*Probe* reads EVM from a file.

*Detector* triggers a ScrambleSituation whenever a jamming attack is detected.

*Decider* triggers switching of *Channel* (see deauthentication) and *AppLayer*.

*Actuator* Sets quality of *AppLayer* protocols.

"""
