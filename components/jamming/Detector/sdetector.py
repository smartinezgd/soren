#    sdetector.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import soren.detector.detector as detector

class JammingDetector(detector.Detector):
    """Detector fo jamming scenario

    Scans measures every 1.5s and creates a Jamming Situation if mean
    EVM over last 3 seconds is lower than 17.5
    """
    def __init__(self,name):
        super().__init__(name,3,shift=1.5)
        self.sleep_time = 1.5
        self.needed_evm = 17.5
        self.evm_reset_floor = 18

    def update(self):
        """Reads EVMMeasure markers and calculates mean EVM over last 3 seconds"""
        allEvm = self.get_measures("EVMMeasure",{})
        l = [e.get_field("evm") for e in allEvm]
        if len(l) > 0:
            return sum(l)/len(l)
        else:
            return 100
        
    def main(self):
        """Creates a ScrambleSituation if needed"""
        if self.bundle < self.needed_evm:
            self.check_and_trigger_situation("Jamming")
        elif self.bundle >= self.evm_reset_floor:
            situations = self.get_situations("Jamming")
            for s in situations:
                self.solve_situation(s)
  
