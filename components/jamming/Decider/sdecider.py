#    sdecider.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import soren.decider.decider as decider
import time

class JammingDecider(decider.Decider):
    """Decider for the jamming scenario
    
    Scans situation each 200ms and triggers the appropriate response :

    - *ConSwitchResponse* if DeauthSituation and *Channel* is set on
       Wi-Fi or if Jamming and *Channel* is set on LTE.

    - *AppLayerReinforceResponse* if DeauthSituation and Jamming.

    The deciders also generates responses to keep *Channel* on Wi-Fi
    and *AppLayer* on economic as much as possible.

    """
    def __init__(self,name):
        super().__init__(name,["DeauthSituation","Jamming"],["ConSwitchResponse","AppLayerReinforceResponse"])
        self.sleep_time = 0.2

    def main(self):
        """Intelligence of the component
        
        Creates appropriate responses after scanning situations"""
        deauth = False
        jamming = False
        channel = self.get_part("protocol").nature
        applayer = self.get_part("applayer").strength
        if len(self.bundle["situations"]) == 0:
            if channel == "4G":
                self.check_and_request_response("ConSwitchResponse",target="Wifi")
            if applayer == "strong":
                self.check_and_request_response("AppLayerReinforceResponse",target="economic")
            self.abort_response("ConSwitchResponse",target="4G")
            self.abort_response("AppLayerReinforceResponse",target="reinforce")
        else:
            for situation in self.bundle["situations"]:
                if situation.type_name == "DeauthSituation":
                    deauth = True
                elif situation.type_name == "Jamming":
                    jamming = True
            if deauth and not jamming:
                if channel == "Wifi":
                    self.check_and_request_response("ConSwitchResponse",target="4G")
                if applayer == "strong":
                    self.check_and_request_response("AppLayerReinforceResponse",target="economic")              
                self.abort_response("Conswitchresponse",target="Wifi")
                self.abort_response("AppLayerReinforceResponse",target="reinforce")
            elif jamming and not deauth:
                if channel == "4G":
                    self.check_and_request_response("ConSwitchResponse",target="Wifi")
                if applayer == "strong":
                    self.check_and_request_response("AppLayerReinforceResponse",target="economic")
                self.abort_response("Conswitchresponse",target="4G")
                self.abort_response("AppLayerReinforceResponse",target="reinforce")
            elif deauth and jamming:
                if channel == "4G":
                    self.check_and_request_response("ConSwitchResponse",target="Wifi")
                if applayer == "economic":           
                    self.check_and_request_response("AppLayerReinforceResponse",target="reinforce")
                self.abort_response("Conswitchresponse",target="4G")
                self.abort_response("AppLayerReinforceResponse",target="economic")





