#    sprobe.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import soren.probe.probe as probe
from datetime import datetime

class JammingProbe(probe.Probe):
    """Probe for the jamming scenario

    Reads a EVM measure each 200ms from trace"""
    def __init__(self,name,trace):
        self.trace = trace
        self.open_trace()
        super().__init__(name)
        self.sleep_time = 0.2

    def open_trace(self):
        """Opens trace file"""
        #open trace
        self.traceptr = open(self.trace,"r")
        
    def stop_hook(self):
        """Close trace file"""
        #close trace
        self.traceptr.close()
        
    def main(self):
        """Read an EVM measure from trace a creates a EVMMeasure"""
        line = self.traceptr.readline()
        if line == "":
            print("scramble restart")
            self.traceptr.close()
            self.traceptr = open(self.trace,"r")
        else:
            evm = line.strip()
            date = datetime.now()
            return self.create_measure("EVMMeasure",date=date,evm=float(evm))
   
