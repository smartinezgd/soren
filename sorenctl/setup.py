#!/usr/bin/env python3

from distutils.core import setup
import os

icons=["sorenctl/ui/icons/*"]

setup(name="sorenctl",
      version="1.0",
      description="SoREn graphical reconfiguration tool",
      author="Sébastien Martinez",
      author_email="sebastien.martinez@ifsttar.fr",
      scripts=["bin/sorenctl"],
      packages=["sorenctl","sorenctl.ui"],
      package_dir={"sorenctl.ui": "sorenctl/ui"},
      package_data = {'sorenctl.ui' : ["icons/*.png"] },
      license="special",
)
      
