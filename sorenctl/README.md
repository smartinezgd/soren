#Sorenctl

A Graphical interface for reconfiguring SoREn

##Requirements

- **Python3-yaml** for parsing and writing configuration files
- **Python3-Qt5** for the graphical interface

##Installation

Run `python3 setup.py install`

You may specify a __--prefix__ option with a path in your PYTHONPATH

##Running

run `sorenctl`


  
  




