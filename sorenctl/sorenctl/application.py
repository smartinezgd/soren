#    application.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.


import os
import yaml
import traceback
import socket
import re


class Scenario(object):
    def __init__(self,name,path):
        self.name = name
        self.path = path
        self.components = []
        self.links = []
        self.saved = True #True if state in path is same as here

    def is_saved(self):
        return self.saved
        
    def has_component_named(self,name):
        for comp in self.components:
            if comp.name == name:
                return True
        return False

    def delete_component_if_exists(self,comp):
        if comp in self.components:
            self.components.remove(comp)
            self.saved = False
            return True
        return False
    
    def get_all_marker_types(self):
        mt = []
        for comp in self.components:
            for m in comp.supplies:
               if m not in mt:
                   mt.append(m)
            for m in comp.reads:
               if m not in mt:
                   mt.append(m)
        return mt

    def add_component(self,name,ctype,path,parameters):
        c = Component(name,ctype,path,parameters)
        self.components.append(c)
        self.saved = False
        return c.parse_manifest()

    def add_link(self,source,target,mtypes):
        self.links.append(Link(source,target,mtypes))
        self.saved = False

    def get_component(self,name):
        for comp in self.components:
            if comp.name == name:
                return comp
        return None
    
        
    def content_toYaML(self):
        d = {"probes":{},"detectors":{},"deciders":{},"actuators":{},"parts":{},"links":[]}
        for comp in self.components:
            if comp.ctype == "probe":
                d["probes"][comp.name] = comp.to_YaML()
            elif comp.ctype == "detector":
                d["detectors"][comp.name] = comp.to_YaML()
            elif comp.ctype == "decider":
                d["deciders"][comp.name] = comp.to_YaML()
            elif comp.ctype == "actuator":
                d["actuators"][comp.name] = comp.to_YaML()
            elif comp.ctype == "part":
                d["parts"][comp.name] = comp.to_YaML()
        for lnk in self.links:
            d["links"].append(str(lnk))
        return d
        
    def to_YaML(self):
        self.saved = True
        if self.path:
            self.save()
            return self.path
        else:
            return self.content_toYaML()
            
    def save(self):
        f = open(self.path,"w")
        yaml.dump(self.content_toYaML(),f,default_flow_style=False)
        f.close()
        self.saved = True

    @classmethod
    def extract_from_file(cls,path):
        if not os.path.exists(path):
            return None
        conf = None
        with open(path,"r") as cfgfile:
            conf = yaml.safe_load(cfgfile)
        return conf
        
    @classmethod
    def from_YaML(cls,name,doc):
        content = None
        path = None
        if isinstance(doc,str) and doc.endswith(".yaml"):
            #load from Yaml file
            path = doc
            content = cls.extract_from_file(path)
        elif isinstance(doc,dict):
            content = doc
        if content != None:
            components = []
            links = []
            for comp_mtype in ["probes","detectors","deciders","actuators","parts"]:
                if comp_mtype in content.keys():
                    for comp in content[comp_mtype]:
                        c = Component.from_YaML(comp,content[comp_mtype][comp],comp_mtype.rstrip("s"))
                        if c:
                            components.append(c)
            if "links" in content.keys():
                for lnk in content["links"]:
                    l = Link.from_string(lnk)
                    if l:
                        links.append(l)
            sc = Scenario(name,path)
            sc.components = components
            sc.links = links
            return sc
        else:
            return None
        

class Link(object):
    parse = re.compile(r"([^ ]+) to ([^ ]+) with (.*)")
    
    def __init__(self,source,target,marker_types):
        self.source = source
        self.target = target
        self.marker_types = marker_types

    def __str__(self):
        return str(self.source)+" to "+str(self.target)+" with "+",".join(self.marker_types)

    @classmethod
    def from_string(cls,string):
        m = Link.parse.match(string)
        if m:
            source = m.group(1)
            target = m.group(2)
            mtypes = [n.strip() for n in m.group(3).split(",")]
            return Link(source,target,mtypes)
        return None
            

class Component(object):
    def __init__(self,ctype,name,path,parameters):
        self.name = name
        self.ctype = ctype
        self.path = path
        self.parameters = parameters
        self.supplies = []
        self.reads = []

    def parse_manifest(self):
        manifestpath = os.path.join(self.path,"MANIFEST.yaml")
        if not os.path.exists(manifestpath):
            return False
        with open(manifestpath,"r") as mftfile:
            manifest = yaml.safe_load(mftfile)
            if "supplies" in manifest.keys() and isinstance(manifest["supplies"],dict):
                for marker_type in manifest["supplies"].keys():
                    self.supplies.append(marker_type)
            if "reads" in manifest.keys() and isinstance(manifest["reads"],dict):
                for marker_type in manifest["reads"].keys():
                    self.reads.append(marker_type)
        return True
        
    def to_YaML(self):
        d = {"path":self.path}
        if len(self.parameters) > 0:
            d["parameters"] = self.parameters
        return d

    @classmethod
    def from_YaML(cls,name,doc,ctype):
        path = None
        parameters = {}
        if "path" in doc.keys():
            path = doc["path"]
        if "parameters" in doc.keys():
            parameters = doc["parameters"]
        if path:
            c = Component(ctype,name,path,parameters)
            c.parse_manifest()
            return c
        return None
    
class Application(object):
    def __init__(self):
        self.reset()

    def reset(self):
        self.saved = True
        self.scenarii = []
        self.verbosity = 2
        self.logfile = "soren.log"
        self.config_file = None
        self.platform_host = "localhost"
        self.platform_port= 4242        

    def has_component_named(self,name):
        for scenario in self.scenarii:
            if scenario.has_component_named(name):
                return True
        return False

    def delete_component(self,comp):
        for scenario in self.scenarii:
            if scenario.delete_component_if_exists(comp):
                self.saved = False
                return

    def get_component(self,name):
        for scenario in self.scenarii:
            c = scenario.get_component(name)
            if c:
                return c
        return None

    def new_scenario(self,name,path):
        d = {}
        if path:
            d = path
        sc = Scenario.from_YaML(name,d)
        if sc:
            self.scenarii.append(sc)
            self.saved = False
                          
    def is_saved(self):
        if self.saved:
            for scenario in self.scenarii:
                if not scenario.is_saved():
                    return False
        return self.saved
        
    def new_config(self):
        self.reset()

    def write_config(self):
        if not self.config_file:
            return "unset"
        #Save
        try:
            f = open(self.config_file,"w")
            yaml.dump(self.to_YaML(),f,default_flow_style=False)
            f.close()
        except Exception as e:
            return str(e)
        self.saved = True
        return "ok"

    def write_config_to(self,conf_path):
        try:
            f = open(conf_path,"w")
            f.close()
        except Exception as e:
            return str(e)
        self.config_file = conf_path
        return self.write_config()

    def open_config(self,conf_path):
        self.reset()
        try:
            with open(conf_path,"r") as cfgfile:
                conf = yaml.safe_load(cfgfile)
                if "soren" in conf.keys():
                    logconf = conf["soren"]
                    if "port" in logconf.keys():
                        self.platform_port = int(logconf["port"])
                    if "loglevel" in logconf.keys():
                        self.verbosity = int(logconf["loglevel"])
                    if "host" in logconf.keys():
                        self.platform_host = logconf["host"]
                    if "logfile" in logconf.keys():
                        self.logfile = logconf["logfile"]
                for scenario in conf.keys():
                    if scenario != "soren":
                        sc= Scenario.from_YaML(scenario,conf[scenario])
                        if sc:
                            self.scenarii.append(sc)
        except Exception as e:
            return str(e)
        self.saved = True
        self.config_file = conf_path
        return "ok"

    def set_verbosity(self,v):
        if v > 0:
            self.verbosity = v

    def set_logfile(self,lf):
        if os.path.exists(os.path.dirname(lf)):
            self.logfile = lf

    def set_platform_host(self,host):
        self.platform_host = host

    def set_platform_port(self,port):
        self.platform_port = port

    def to_YaML(self):
        doc = {"soren":{"port":self.platform_port,"loglevel":self.verbosity,"host":self.platform_host,"logfile":self.logfile}}
        for scenario in self.scenarii:
            doc[scenario.name] = scenario.to_YaML()
        return doc

    def log_command(self):
        return "set logfile="+self.logfile+" loglevel="+str(self.verbosity)

    def send(self,comm):
        soc = socket.socket(socket.AF_INET,socket.SOCK_STREAM)
        soc.connect((self.platform_host,self.platform_port))
        soc.sendall(comm.encode("ascii"))
        soc.close()
    
    def push(self):
        #init soket
        try:
            self.send("apply "+os.path.abspath(self.config_file))
        except Exception as e:
            return str(e)
        return ""                    

    def check_validity(self):
        #returns "ok" if valid, else, returns a string explaining what is not valid
        #check if two elements have the same name
        two_elem_same_name = False
        names = []
        for scenario in self.scenarii:
            if not two_elem_same_name:
                for comp in scenario.components:
                    if comp.name in names:
                        two_elem_same_name = True
                        break
                    else:
                        names.append(comp.name)
        two_scenario_same_name = False
        names = []
        for scenario in self.scenarii:
            if scenario.name in names:
                two_scenario_same_name = True
                break
            else:
                names.append(scenario.name)
        s = []
        if two_elem_same_name:
            s.append("- Two elements share a same name")
        if two_scenario_same_name:
            s.append("- Two scenarii share a same name")
        if len(s) > 0:
            return "\n".join(s)
        else:
            return "ok"
