#    ui/addscenario.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from PyQt5.QtWidgets import QMainWindow, QWidget, QHBoxLayout, QVBoxLayout, QLabel, QComboBox, QLineEdit, QPushButton, QFileDialog, QDialog, QTableWidget, QTableWidgetItem
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt,pyqtSignal
from sorenctl.ui.widgets import gen_yes_no_dialog,gen_info_dialog
from sorenctl.ui.icons import get_icons_path
import os

icons = get_icons_path()

class AddScenarioWindow(QMainWindow):
    close_sig = pyqtSignal()
    
    def __init__(self,parent):
        super().__init__(parent)
        self.app = parent.app
        self.title = "New Scenario"
        self.left = 50
        self.top = 60
        self.width = 180
        self.height = 200
        self.setWindowTitle(self.title)
        self.setGeometry(self.left,self.top,self.width,self.height)
        self.central = AddScenarioCentral(self)
        self.setCentralWidget(self.central)

    def close_w(self):
        self.close_sig.emit()
        self.close()

        
class AddScenarioCentral(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.app = parent.app
        self.win = parent
        self.path = ""
        self.name = ""
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QVBoxLayout()
        self.add_label()
        self.add_name_input()
        self.add_path_input()
        self.add_buttons()
        self.setLayout(self.layout)


    def add_label(self):
        self.label = QLabel(self)
        self.label.setFont(self.bold)
        self.label.setText("Leave path empty to create new scenario\nSelect scenario file to import existing scenario.")
        self.layout.addWidget(self.label)

    def add_name_input(self):
        self.name_input = QWidget(self)
        self.name_input.layout = QHBoxLayout()
        self.name_input.label = QLabel(self)
        self.name_input.label.setFont(self.bold)
        self.name_input.label.setText("Name : ")
        self.name_input.layout.addWidget(self.name_input.label)
        self.name_input.lineedit = QLineEdit(self)
        self.name_input.lineedit.textChanged.connect(self.set_name)
        self.name_input.layout.addWidget(self.name_input.lineedit)
        self.name_input.setLayout(self.name_input.layout)
        self.layout.addWidget(self.name_input)

    def set_name(self):
        self.name = self.name_input.lineedit.text().strip()
    
    def add_path_input(self):
        self.path_input = QWidget(self)
        self.path_input.layout = QHBoxLayout()
        self.path_input.label = QLabel(self)
        self.path_input.label.setFont(self.bold)
        self.path_input.label.setText("Path : ")
        self.path_input.layout.addWidget(self.path_input.label)
        self.path_input.selector = QPushButton("Select scenario path",self)
        self.path_input.selector.clicked.connect(self.set_path)
        self.path_input.layout.addWidget(self.path_input.selector)
        self.path_input.setLayout(self.path_input.layout)
        self.layout.addWidget(self.path_input)

    def set_path(self):
        path = QFileDialog.getOpenFileName(self, 'Select path to scenario', '.',filter=('YaML (*.yaml)'))[0]
        if path:
            if len(path) > 30:
                self.path_input.selector.setText("..."+path[-27:])
            else:
                self.path_input.selector.setText(path)
            self.path = path
        
    def add_buttons(self):
        self.buttons = QWidget(self)
        self.buttons.layout = QHBoxLayout()
        self.buttons.cancel = QPushButton("Cancel",self.buttons)
        self.buttons.cancel.clicked.connect(self.cancel)
        self.buttons.layout.addWidget(self.buttons.cancel)
        self.buttons.ok = QPushButton("Validate",self.buttons)
        self.buttons.ok.clicked.connect(self.validate)
        self.buttons.layout.addWidget(self.buttons.ok)
        self.buttons.setLayout(self.buttons.layout)
        self.layout.addWidget(self.buttons)

    def clear(self):
        self.name = ""
        self.path = ""
        self.name_input.lineedit.setText(self.name)
        self.path_input.selector.setText("Select path to scenario")
        
    def validate(self):
        #Do stuff
        if self.name == "":
            gen_info_dialog(self,"Error","No name specified for scenario!")
        else:
            self.app.new_scenario(self.name,self.path)
            validity = self.app.check_validity()
            if validity != "ok":
                gen_info_dialog(self,"Warning",'Configuration is not valid for the following reasons :\n\n'+validity+'\n\nYou may want to correct it')
            self.clear()
            self.win.close_w()

    def cancel(self):
        self.clear()
        self.win.close_w()

