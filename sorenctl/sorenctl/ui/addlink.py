#    ui/addlink.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from PyQt5.QtWidgets import QMainWindow, QWidget, QHBoxLayout, QVBoxLayout, QLabel, QComboBox, QPushButton, QFileDialog, QDialog, QListWidget,QAbstractItemView
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt,pyqtSignal,QItemSelectionModel
from sorenctl.ui.widgets import gen_yes_no_dialog,gen_info_dialog
from sorenctl.ui.icons import get_icons_path
import os

icons = get_icons_path()

class AddLinkWindow(QMainWindow):
    close_sig = pyqtSignal()
    
    def __init__(self,parent):
        super().__init__(parent)
        self.app = parent.app
        self.title = "Adding a link"
        self.left = 50
        self.top = 60
        self.width = 280
        self.height = 400
        self.setWindowTitle(self.title)
        self.setGeometry(self.left,self.top,self.width,self.height)
        self.central = AddLinkCentral(self)
        self.setCentralWidget(self.central)

    def close_w(self):
        self.close_sig.emit()
        self.close()

    def edit(self,link):
        self.central.edit(link)

    def set_scenario(self,scenario):
        self.central.scenario = scenario
        self.central.update()
        
        
class AddLinkCentral(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.app = parent.app
        self.win = parent
        self.source = ""
        self.target = ""
        self.mtypes = []
        self.scenario = None
        self.link = None
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QVBoxLayout()
        self.add_source_input()
        self.add_target_input()
        self.add_mtypes_input()
        self.add_buttons()
        self.setLayout(self.layout)
        self.editting = False

    def update(self):
        for comp in self.scenario.components:
            if comp.ctype != "part":
                if not self.source:
                    self.source = comp.name
                if not self.target:
                    self.target = comp.name
                self.source_input.selector.addItem(comp.name)
                self.target_input.selector.addItem(comp.name)
        l = self.scenario.get_all_marker_types()
        for m in l:
            self.mtypes_input.selector.addItem(m)
        
    def edit(self,link):
        self.editting = True
        self.link = link
        self.source = link.source
        self.target = link.target
        self.mtypes = link.marker_types
        #Update inputs
        complist = [comp.name for comp in self.scenario.components]
        self.source_input.selector.setCurrentIndex(complist.index(link.source))
        self.target_input.selector.setCurrentIndex(complist.index(link.target))
        l = self.scenario.get_all_marker_types()
        for n in link.marker_types:
            if n in l:
                self.mtypes_input.selector.item(l.index(n)).setSelected(True)

    def add_source_input(self):
        self.source_input = QWidget(self)
        self.source_input.layout = QHBoxLayout()
        self.source_input.label = QLabel(self)
        self.source_input.label.setFont(self.bold)
        self.source_input.label.setText("Source : ")
        self.source_input.layout.addWidget(self.source_input.label)
        self.source_input.selector = QComboBox(self)
        self.source_input.selector.activated.connect(self.set_source)
        self.source_input.setLayout(self.source_input.layout)
        self.source_input.layout.addWidget(self.source_input.selector)
        self.layout.addWidget(self.source_input)

    def set_source(self):
        self.source = self.source_input.selector.currentText()

    def add_target_input(self):
        self.target_input = QWidget(self)
        self.target_input.layout = QHBoxLayout()
        self.target_input.label = QLabel(self)
        self.target_input.label.setFont(self.bold)
        self.target_input.label.setText("Target : ")
        self.target_input.layout.addWidget(self.target_input.label)
        self.target_input.selector = QComboBox(self)
        self.target_input.selector.activated.connect(self.set_target)
        self.target_input.setLayout(self.target_input.layout)
        self.target_input.layout.addWidget(self.target_input.selector)
        self.layout.addWidget(self.target_input)

    def set_target(self):
        self.target = self.target_input.selector.currentText()

    def add_mtypes_input(self):
        self.mtypes_input = QWidget(self)
        self.mtypes_input.layout = QHBoxLayout()
        self.mtypes_input.label = QLabel(self)
        self.mtypes_input.label.setFont(self.bold)
        self.mtypes_input.label.setText("Marker Types : ")
        self.mtypes_input.layout.addWidget(self.mtypes_input.label)
        self.mtypes_input.selector = QListWidget(self)
        self.mtypes_input.selector.setSelectionMode(QAbstractItemView.MultiSelection)
        self.mtypes_input.selector.itemClicked.connect(self.set_mtypes)
        self.mtypes_input.layout.addWidget(self.mtypes_input.selector)
        self.mtypes_input.setLayout(self.mtypes_input.layout)
        self.layout.addWidget(self.mtypes_input)

    def set_mtypes(self,item):
        n = item.text()
        if n in self.mtypes:
            self.mtypes.remove(n)
        else:
            self.mtypes.append(n)
            
    def add_buttons(self):
        self.buttons = QWidget(self)
        self.buttons.layout = QHBoxLayout()
        self.buttons.cancel = QPushButton("Cancel",self.buttons)
        self.buttons.cancel.clicked.connect(self.cancel)
        self.buttons.layout.addWidget(self.buttons.cancel)
        self.buttons.ok = QPushButton("Validate",self.buttons)
        self.buttons.ok.clicked.connect(self.validate)
        self.buttons.layout.addWidget(self.buttons.ok)
        self.buttons.setLayout(self.buttons.layout)
        self.layout.addWidget(self.buttons)

    def clear(self):
        self.source = ""
        self.target = ""
        self.mtypes = []
        self.link = None
        self.scenario = None
        self.source_input.selector.clear()
        self.target_input.selector.clear()
        self.mtypes_input.selector.clear()
        self.editting = False
        
    def validate(self):
        if self.source == self.target:        
            gen_info_dialog(self,"Error!","Link has same source and target")
        elif len(self.mtypes) < 1:
            gen_info_dialog(self,"Error!","Link has no marker type")            
        else:
            if self.editting:
                self.link.source = self.source
                self.link.target = self.target
                self.link.marker_types = self.mtypes
            else:
                self.scenario.add_link(self.source,self.target,self.mtypes)
            self.clear()
            self.win.close_w()

    def cancel(self):
        self.clear()
        self.win.close_w()

