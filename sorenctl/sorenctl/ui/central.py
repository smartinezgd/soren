#    ui/central.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from PyQt5.QtWidgets import QWidget, QTreeWidget, QTreeWidgetItem, QVBoxLayout, QHBoxLayout, QLabel, QAction, QMenu, QTabWidget,QLineEdit,QPushButton,QFileDialog
from PyQt5.QtGui import QPixmap,QFont,QCursor,QBrush
from PyQt5.QtCore import Qt
from sorenctl.ui.icons import get_icons_path
import os

icons = get_icons_path()


class CentralView(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.app = parent.app
        self.layout = QVBoxLayout()
        self.globview = GlobalView(self)
        self.info = GeneralInfo(self)
        self.layout.addWidget(self.info)
        self.layout.addWidget(self.globview)
        self.setLayout(self.layout)

    def update_globview(self):
        self.globview.update()
        
    def update_gen(self):
        self.info.update()

    def get_current_element(self):
        return self.globview.get_current()

    def get_current_scenario(self):
        return self.globview.get_scenario()

class GeneralInfo(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.app = parent.app
        self.layout = QVBoxLayout()
        self.layout.setContentsMargins(5, 0, 5, 0)
        self.plat = PlatInfo(self)
        self.layout.addWidget(self.plat)
        self.log = LogInfo(self)
        self.layout.addWidget(self.log)
        self.setLayout(self.layout)

    def update(self):
        self.plat.update()
        self.log.update()
        
class BaseInfo(QWidget):
    def __init__(self,parent,pic):
        super().__init__(parent)
        self.app = parent.app
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QHBoxLayout()
        self.layout.setContentsMargins(5, 0, 5, 0)
        self.layout.setAlignment(Qt.AlignLeft)
        self.pic = QLabel(self)
        self.pic.setPixmap(QPixmap(os.path.join(icons,pic)).scaledToWidth(64))
        self.layout.addWidget(self.pic)
        self.textwidget = self.gen_text()
        self.update()
        self.layout.addWidget(self.textwidget)
        self.setLayout(self.layout)

    def gen_text(self):
        root = QWidget(self)
        root.layout = QVBoxLayout()
        root.wid1 = QWidget(root)
        root.wid1.layout = QHBoxLayout()
        root.wid1.layout.setAlignment(Qt.AlignLeft)
        root.wid1.label1 = QLabel(root.wid1)
        root.wid1.label1.setFont(self.bold)
        root.wid1.label1.setText(self.first_key)
        root.wid1.layout.addWidget(root.wid1.label1)
        root.wid1.label2 = QLabel(root.wid1)
        root.wid1.layout.addWidget(root.wid1.label2)
        root.wid1.setLayout(root.wid1.layout)
        root.layout.addWidget(root.wid1)
        root.wid2 = QWidget(root)
        root.wid2.layout = QHBoxLayout()
        root.wid2.layout.setAlignment(Qt.AlignLeft)
        root.wid2.label1 = QLabel(root.wid2)
        root.wid2.label1.setFont(self.bold)
        root.wid2.label1.setText(self.second_key)
        root.wid2.layout.addWidget(root.wid2.label1)
        root.wid2.label2 = QLabel(root.wid1)
        root.wid2.layout.addWidget(root.wid2.label2)
        root.wid2.setLayout(root.wid2.layout)
        root.layout.addWidget(root.wid2)
        root.setLayout(root.layout)
        return root

   
    def update(self):
        pass

    
class LogInfo(BaseInfo):
    def __init__(self,parent):
        self.first_key = "Log Level : "
        self.second_key = "Log File : "
        super().__init__(parent,"log64.png")

    def update(self):
        if self.app.verbosity == 0:
            self.textwidget.wid1.label2.setText("0 (Nothing)")
        elif self.app.verbosity == 1:
            self.textwidget.wid1.label2.setText("1 (Only critical information)")
        elif self.app.verbosity == 2:
            self.textwidget.wid1.label2.setText("2 (Key information)")
        elif self.app.verbosity > 2:
            self.textwidget.wid1.label2.setText("3 (Everything)")
        self.textwidget.wid2.label2.setText(os.path.abspath(self.app.logfile))


        
class PlatInfo(BaseInfo):
    def __init__(self,parent):
        self.first_key = "Platform Host : "
        self.second_key = "Platform Port : "
        super().__init__(parent,"net64.png")

    def update(self):
        self.textwidget.wid1.label2.setText(self.app.platform_host)
        self.textwidget.wid2.label2.setText(str(self.app.platform_port))
        
        
class GlobalView(QTabWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.app = parent.app
        self.scenario_tab = None
        self.tabs = []
        self.currentChanged.connect(self.switch_tab)
        self.update()

    def clear(self):
        super().clear()
        #for tab in self.tabs:
        #    self.removeTab(tab)
        self.tabs = []
        
    def update(self):
        self.clear()
        i = 0
        for scenario in self.app.scenarii:
            tab = ScenarioTab(self,scenario,i)
            self.tabs.append(tab)
            self.addTab(tab,scenario.name)
            i+=1
            if not self.scenario_tab:
                self.scenario_tab = tab
                
    def switch_tab(self,i):
        self.scenario_tab = self.tabs[i]

    def get_scenario(self):
        if self.scenario_tab:
            return self.scenario_tab.scenario
        else:
            return None
        
    def get_current(self):
        if self.scenario_tab:
            return self.scenario_tab.current
        else:
            return None

class ScenarioTab(QWidget):
    def __init__(self,parent,scenario,index):
        super().__init__(parent)
        self.scenario = scenario
        self.index = index
        self.tabview = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QVBoxLayout()
        self.gen_inputs()
        self.comp_label = QLabel(self)
        self.comp_label.setFont(self.bold)
        self.comp_label.setText("Components and Parts")
        self.layout.addWidget(self.comp_label)
        self.comps = ScenarioComp(self,scenario)
        self.layout.addWidget(self.comps)
        self.link_label = QLabel(self)
        self.link_label.setFont(self.bold)
        self.link_label.setText("Links")
        self.layout.addWidget(self.link_label)
        self.links = ScenarioLinks(self,scenario)
        self.layout.addWidget(self.links)
        self.setLayout(self.layout)
        self.current = None
        self.update()


    def gen_inputs(self):
        self.inputs = QWidget(self)
        self.inputs.layout = QVBoxLayout()
        self.inputs.name_widgt = QWidget(self)
        self.inputs.name_widgt.layout = QHBoxLayout()
        self.inputs.name_widgt.label = QLabel(self)
        self.inputs.name_widgt.label.setFont(self.bold)
        self.inputs.name_widgt.label.setText("Scenario Name : ")
        self.inputs.name_widgt.layout.addWidget(self.inputs.name_widgt.label)
        self.inputs.name_widgt.selector = QLineEdit(self)
        self.inputs.name_widgt.selector.textChanged.connect(self.set_name)
        self.inputs.name_widgt.layout.addWidget(self.inputs.name_widgt.selector)
        self.inputs.name_widgt.setLayout(self.inputs.name_widgt.layout)
        self.inputs.layout.addWidget(self.inputs.name_widgt)
        self.inputs.path_widgt = QWidget(self)
        self.inputs.path_widgt.layout = QHBoxLayout()
        self.inputs.path_widgt.label = QLabel(self)
        self.inputs.path_widgt.label.setFont(self.bold)
        self.inputs.path_widgt.label.setText("Scenario Path : ")
        self.inputs.path_widgt.layout.addWidget(self.inputs.path_widgt.label)
        self.inputs.path_widgt.selector = QPushButton("Select path",self)
        self.inputs.path_widgt.selector.clicked.connect(self.set_path)
        self.inputs.path_widgt.layout.addWidget(self.inputs.path_widgt.selector)
        self.inputs.path_widgt.setLayout(self.inputs.path_widgt.layout)
        self.inputs.layout.addWidget(self.inputs.path_widgt)
        self.inputs.setLayout(self.inputs.layout)
        self.layout.addWidget(self.inputs)

    def set_name(self):
        self.scenario.name = self.inputs.name_widgt.selector.text().strip()
        self.tabview.setTabText(self.index,self.scenario.name)

    def set_path(self):
        path = QFileDialog.getSaveFileName(self, 'Select scenario save path', '.',filter=('YaML (*.yaml)'))[0]
        if path:
            if not path.endswith(".yaml"):
                path+=".yaml"
            self.scenario.path = path
            if len(path) > 30:
                self.inputs.path_widgt.selector.setText("..."+path[-27:])
            else:
                self.inputs.path_widgt.selector.setText(path)
        
    def update(self):
        self.inputs.name_widgt.selector.setText(self.scenario.name)
        if self.scenario.path:
            if len(self.scenario.path) > 30:
                self.inputs.path_widgt.selector.setText("..."+self.scenario.path[-27:])
            else:
                self.inputs.path_widgt.selector.setText(self.scenario.path)
        else:
            self.inputs.path_widgt.selector.setText("Select scenario path")
        self.comps.update()
        self.links.update()
    
       
class ScenarioComp(QTreeWidget):
    def __init__(self,parent,scenario):
        super().__init__(parent)
        self.scenario = scenario
        self.tab = parent
        self.setHeaderLabels(["Meta-Type","Name","Path","Parameters"])
        self.setColumnWidth(0,130)
        self.setColumnWidth(1,150)
        self.setColumnWidth(2,130)
        self.itemPressed.connect(self.get_current)
        self.update()
        
    def update(self):
        self.clear()
        probes = QTreeWidgetItem(self)
        probes.setText(0,"Probes")
        probes.setFlags(Qt.ItemIsEnabled)
        probes.setExpanded(True)
        detectors = QTreeWidgetItem(self)
        detectors.setText(0,"Detectors")
        detectors.setFlags(Qt.ItemIsEnabled)
        detectors.setExpanded(True)
        deciders = QTreeWidgetItem(self)
        deciders.setText(0,"Deciders")
        deciders.setFlags(Qt.ItemIsEnabled)
        deciders.setExpanded(True)
        actuators = QTreeWidgetItem(self)
        actuators.setText(0,"Actuators")
        actuators.setFlags(Qt.ItemIsEnabled)
        actuators.setExpanded(True)
        parts = QTreeWidgetItem(self)
        parts.setText(0,"Parts")
        parts.setFlags(Qt.ItemIsEnabled)
        parts.setExpanded(True)
        for comp in self.scenario.components:
            if comp.ctype == "probe":
                self.add_component(probes,comp)
            elif comp.ctype == "detector":
                self.add_component(detectors,comp)
            elif comp.ctype == "decider":
                self.add_component(deciders,comp)
            elif comp.ctype == "actuator":
                self.add_component(actuators,comp)
            elif comp.ctype == "part":
                self.add_component(parts,comp)

    def add_component(self,parent,c):
        ce = QTreeWidgetItem(parent)
        ce.setFlags(Qt.ItemIsEnabled)
        ce.setText(1,c.name)
        ce.setText(2,os.path.basename(c.path))
        ce.component = c
        for key in c.parameters.keys():
            ke = QTreeWidgetItem(ce)
            ke.setFlags(Qt.ItemIsEnabled)
            ke.setText(3,key+" = "+str(c.parameters[key]))
            ke.component = c
        
    def get_current(self,item,col):
        if hasattr(item,"component"):
            self.tab.current = item.component
    

class ScenarioLinks(QTreeWidget):
    def __init__(self,parent,scenario):
        super().__init__(parent)
        self.scenario = scenario
        self.tab = parent
        self.setHeaderLabels(["From","To","Marker Types"])
        self.setColumnWidth(0,150)
        self.setColumnWidth(1,150)
        self.setColumnWidth(2,150)
        self.itemPressed.connect(self.get_current)
        self.update()
        
    def update(self):
        self.clear()
        for lnk in self.scenario.links:
            le = QTreeWidgetItem(self)
            le.setFlags(Qt.ItemIsEnabled)
            le.setText(0,lnk.source)
            le.setText(1,lnk.target)
            le.link = lnk
            for mtype in lnk.marker_types:
                mte = QTreeWidgetItem(le)
                mte.setFlags(Qt.ItemIsEnabled)
                mte.setText(2,mtype)
                mte.link = lnk
                
    def get_current(self,item,col):
        if hasattr(item,"link"):
            self.tab.current = item.link
