#    ui/mainwindow.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from PyQt5.QtWidgets import QMainWindow,QAction,qApp,QMessageBox,QFileDialog
from PyQt5.QtGui import QIcon
from sorenctl.ui.widgets import gen_quit_dialog,gen_info_dialog,gen_yes_no_dialog
from sorenctl.ui.central import CentralView
from sorenctl.ui.conf import ConfWindow
from sorenctl.ui.addcomp import AddCompWindow
from sorenctl.ui.addpart import AddPartWindow
from sorenctl.ui.addlink import AddLinkWindow
from sorenctl.ui.addscenario import AddScenarioWindow
from sorenctl.application import Component
import os
from sorenctl.ui.icons import get_icons_path

icons = get_icons_path()



class MainWindow(QMainWindow):
    def __init__(self,application):
        super().__init__()
        self.title = "SoREn control interface"
        self.app = application
        self.left = 30
        self.top = 30
        self.width = 700
        self.height = 800
        self.setWindowTitle(self.title)
        self.setGeometry(self.left,self.top,self.width,self.height)
        self.create_toolbar() #Init toolbar
        self.central_view = CentralView(self)
        self.setCentralWidget(self.central_view)
        self.confwin = ConfWindow(self)
        self.confwin.close_sig.connect(self.genconf_update)
        self.addComp = AddCompWindow(self)
        self.addComp.close_sig.connect(self.globview_update)
        self.addPart = AddPartWindow(self)
        self.addPart.close_sig.connect(self.globview_update)
        self.addLink = AddLinkWindow(self)
        self.addLink.close_sig.connect(self.globview_update)
        self.addScenario = AddScenarioWindow(self)
        self.addScenario.close_sig.connect(self.globview_update)
        self.show()

    def create_toolbar(self):
        ###########
        # Actions #
        ###########
        #Generic
        exitA = QAction(QIcon(os.path.join(icons,"quit.png")),"Quit",self)
        exitA.triggered.connect(self.quit_method)
        newA = QAction(QIcon(os.path.join(icons,"new.png")),"New configuration",self)
        newA.triggered.connect(self.new_config)
        writeA = QAction(QIcon(os.path.join(icons,"write.png")),"Save configuration",self)
        writeA.triggered.connect(self.write_config)
        writeToA = QAction(QIcon(os.path.join(icons,"write.png")),"Save configuration as",self)
        writeToA.triggered.connect(self.write_config_to)
        openA = QAction(QIcon(os.path.join(icons,"open.png")),"Open configuration",self)
        openA.triggered.connect(self.open_config)
        #Scenarii
        newScenario = QAction(QIcon(os.path.join(icons,"comp.png")),"New Scenario",self)
        newScenario.triggered.connect(self.new_scenario)
        rmScenario = QAction(QIcon(os.path.join(icons,"dcomp.png")),"Remove Scenario",self)
        rmScenario.triggered.connect(self.delete_scenario)
        #Components
        addComponent = QAction(QIcon(os.path.join(icons,"marker.png")),"Add Component",self)
        addComponent.triggered.connect(self.new_component)
        #Parts
        addPart = QAction(QIcon(os.path.join(icons,"part.png")),"Add Part",self)
        addPart.triggered.connect(self.new_part)
        #Links
        addLink = QAction(QIcon(os.path.join(icons,"lmarker.png")),"Add Link",self)
        addLink.triggered.connect(self.new_link)
        #Common
        rm = QAction(QIcon(os.path.join(icons,"dmarker.png")),"Remove Selected Element",self)
        rm.triggered.connect(self.delete_elem)
        edit = QAction(QIcon(os.path.join(icons,"emarker.png")),"Edit Selected Element",self)
        edit.triggered.connect(self.edit_elem)
        #Push
        pushA = QAction(QIcon(os.path.join(icons,"push.png")),"Push configuration",self)
        pushA.triggered.connect(self.push)
        #General config
        confA = QAction(QIcon(os.path.join(icons,"conf.png")),"General configuration",self)
        confA.triggered.connect(self.general_conf)
        #Menu
        self.menubar = self.menuBar()
        fileMenu = self.menubar.addMenu("File")
        fileMenu.addAction(newA)
        fileMenu.addAction(openA)
        fileMenu.addAction(writeA)
        fileMenu.addAction(writeToA)
        fileMenu.addSeparator()
        fileMenu.addAction(exitA)
        consoleMenu = self.menubar.addMenu("Console")
        consoleMenu.addAction(confA)
        consoleMenu.addAction(pushA)
        scenarioMenu = self.menubar.addMenu("Scenario")
        scenarioMenu.addAction(newScenario)
        scenarioMenu.addAction(rmScenario)
        compMenu = self.menubar.addMenu("Elements")
        compMenu.addAction(addComponent)
        compMenu.addAction(addPart)
        compMenu.addAction(addLink)
        compMenu.addSeparator()
        compMenu.addAction(edit)
        compMenu.addAction(rm)
        #Toolbar
        self.toolbar = self.addToolBar("toolbar")
        self.toolbar.addAction(newA)
        self.toolbar.addAction(openA)
        self.toolbar.addAction(writeA)
        self.toolbar.addSeparator()
        self.toolbar.addAction(newScenario)
        self.toolbar.addAction(rmScenario)
        self.toolbar.addSeparator()
        self.toolbar.addAction(addComponent)
        self.toolbar.addAction(addPart)
        self.toolbar.addAction(addLink)
        self.toolbar.addSeparator()
        self.toolbar.addAction(edit)
        self.toolbar.addAction(rm)
        self.toolbar.addSeparator()
        self.toolbar.addAction(confA)
        self.toolbar.addSeparator()
        self.toolbar.addAction(pushA)
        self.toolbar.addSeparator()
        self.toolbar.addAction(exitA)

    def new_config(self):
        if not self.app.is_saved():
            if not gen_yes_no_dialog(self,"Warning","Current configuration unsaved\nAll changes will be lost\nContinue?"):
                return 
        self.app.new_config()
        self.total_update()

    def write_config(self):
        validity = self.app.check_validity()
        if validity != "ok":
            gen_info_dialog(self,"Warning",'Configuration is not valid for the following reasons :\n\n'+validity+'\n\nYou may want to correct it and save again')
        ans = self.app.write_config()
        if ans == "unset":
            self.write_config_to()
        elif ans != "ok":
            gen_info_dialog(self,"Error","Error when saving configuration:\n"+ans)
        
    def write_config_to(self):
        validity = self.app.check_validity()
        if validity != "ok":
            gen_info_dialog(self,"Warning",'Configuration is not valid for the following reasons :\n\n'+validity+'\n\nYou may want to correct it before saving')
        conf = QFileDialog.getSaveFileName(self, 'Save configuration', '.',filter=('YaML (*.yaml)'))[0]
        if conf:
            if not conf.endswith(".yaml"):
                conf+=".yaml"
            ans = self.app.write_config_to(conf)
            if ans != "ok":
                gen_info_dialog(self,"Error","Error when saving configuration:\n"+ans)

    def open_config(self):
        if not self.app.is_saved():
            if not gen_yes_no_dialog(self,"Warning","Current configuration unsaved\nAll changes will be lost\nContinue?"):
                return 
        conf = QFileDialog.getOpenFileName(self, 'Open configuration file', '.',filter=('YaML (*.yaml)'))[0]
        if conf:
            ans = self.app.open_config(conf)
            if ans != "ok":
                gen_info_dialog(self,"Error","Error when opening configuration:\n"+ans)
            else:
                validity = self.app.check_validity()
                if validity != "ok":
                    gen_info_dialog(self,"Warning",'Configuration is not valid for the following reasons :\n\n'+validity+'\n\nYou may want to correct it')
                self.total_update()

    def new_component(self):
        self.addComp.set_scenario(self.central_view.get_current_scenario())
        self.addComp.show()

    def new_part(self):
        self.addPart.set_scenario(self.central_view.get_current_scenario())
        self.addPart.show()

    def new_link(self):
        self.addLink.set_scenario(self.central_view.get_current_scenario())
        self.addLink.show()
        
    def delete_elem(self):
        scenario = self.central_view.get_current_scenario()
        element = self.central_view.get_current_element()
        if scenario and element:
            if type(element) == Component:
                scenario.components.remove(element)
            else:
                scenario.links.remove(element)
            self.globview_update()

    def edit_elem(self):
        scenario = self.central_view.get_current_scenario()
        element = self.central_view.get_current_element()
        if scenario and element:
            if type(element) == Component:
                if element.ctype == "part":
                    self.addPart.set_scenario(scenario)
                    self.addPart.edit(element)
                    self.addPart.show()
                else:
                    self.addComp.set_scenario(scenario)
                    self.addComp.edit(element)
                    self.addComp.show()
            else:
                self.addLink.set_scenario(scenario)
                self.addLink.edit(element)
                self.addLink.show()

    def delete_scenario(self):
        scenario = self.central_view.get_current_scenario()
        if scenario:
            self.app.scenarii.remove(scenario)
            self.globview_update()

    def new_scenario(self):
        self.addScenario.show()

    def push(self):
        if not self.app.saved:
            gen_info_dialog(self,"Error","Please save configuration before pushing")
        else:
            validity = self.app.check_validity()
            if validity != "ok":
                gen_info_dialog(self,"Error",'Configuration is not valid for the following reasons :\n\n'+validity+'\n\nYou need to correct it before pushing')
            else:
                ans = self.app.push()
                if ans:
                    gen_info_dialog(self,"Error","Error when pushing configuration:\n"+ans)
                else:
                    gen_info_dialog(self,"Pushing configuration","Command sent")

    def general_conf(self):
        self.confwin.show()

    def total_update(self):
        self.central_view.update_globview()
        self.central_view.update_gen()
        self.confwin.update()

    def genconf_update(self):
        self.central_view.update_gen()

    def globview_update(self):
        self.central_view.update_globview()
        
    def quit_method(self):
        if not self.app.is_saved():
            reply = gen_quit_dialog(self)
            if reply == "save":
                self.write_config()
                if self.app.is_saved():
                    gen_info_dialog(self,"Quitting","Configuration has been saved")
                    qApp.quit()
            elif reply == "discard":
                qApp.quit()
        else:
            qApp.quit()

    def closeEvent(self,event):
        self.quit_method()
