#    ui/conf.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from PyQt5.QtWidgets import QMainWindow, QWidget, QHBoxLayout, QVBoxLayout, QLabel, QComboBox, QLineEdit, QSpinBox, QPushButton, QFileDialog, QDialog
from PyQt5.QtGui import QPixmap,QFont
from PyQt5.QtCore import Qt,pyqtSignal
import os
from sorenctl.ui.icons import get_icons_path

icons = get_icons_path()



class ConfWindow(QMainWindow):
    close_sig = pyqtSignal()
    
    def __init__(self,parent):
        super().__init__(parent)
        self.app = parent.app
        self.title = "General configuration"
        self.left = 50
        self.top = 60
        self.width = 340
        self.height = 200
        self.setWindowTitle(self.title)
        self.setGeometry(self.left,self.top,self.width,self.height)
        self.central = Central(self)
        self.setCentralWidget(self.central)

    def close_w(self):
        self.close_sig.emit()
        self.close()

    def update(self):
        self.central.update()

        
class Central(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.app = parent.app
        self.win = parent
        self.loglevel = self.app.verbosity
        self.logfile = self.app.logfile
        self.host = self.app.platform_host
        self.port = self.app.platform_port
        self.layout = QVBoxLayout()
        self.plat = PlatInput(self)
        self.layout.addWidget(self.plat)
        self.log = LogInput(self)
        self.layout.addWidget(self.log)
        self.add_buttons()
        self.setLayout(self.layout)

    def update(self):
        self.loglevel = self.app.verbosity
        self.logfile = self.app.logfile
        self.host = self.app.platform_host
        self.port = self.app.platform_port
        self.plat.update()
        self.log.update()
        
    def add_buttons(self):
        self.buttons = QWidget(self)
        self.buttons.layout = QHBoxLayout()
        self.buttons.cancel = QPushButton("Cancel",self.buttons)
        self.buttons.cancel.clicked.connect(self.cancel)
        self.buttons.layout.addWidget(self.buttons.cancel)
        self.buttons.ok = QPushButton("Validate",self.buttons)
        self.buttons.ok.clicked.connect(self.validate)
        self.buttons.layout.addWidget(self.buttons.ok)
        self.buttons.setLayout(self.buttons.layout)
        self.layout.addWidget(self.buttons)

    def validate(self):
        self.app.verbosity = self.loglevel
        self.app.logfile = self.logfile
        self.app.platform_host = self.host
        self.app.platform_port = self.port
        self.win.close_w()

    def cancel(self):
        self.win.close_w()

class BaseInput(QWidget):
    def __init__(self,parent,pic):
        super().__init__(parent)
        self.central = parent
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QHBoxLayout()
        self.layout.setContentsMargins(5, 0, 5, 0)
        self.layout.setAlignment(Qt.AlignLeft)
        self.pic = QLabel(self)
        self.pic.setPixmap(QPixmap(os.path.join(icons,pic)).scaledToWidth(64))
        self.layout.addWidget(self.pic)
        self.set_text()
        self.textwidget = self.gen_text()
        self.layout.addWidget(self.textwidget)
        self.setLayout(self.layout)

    def gen_text(self):
        root = QWidget(self)
        root.layout = QVBoxLayout()
        root.wid1 = QWidget(root)
        root.wid1.layout = QHBoxLayout()
        root.wid1.layout.setAlignment(Qt.AlignLeft)
        root.wid1.label1 = QLabel(root.wid1)
        root.wid1.label1.setFont(self.bold)
        root.wid1.label1.setText(self.first_key)
        root.wid1.layout.addWidget(root.wid1.label1)
        root.wid1.inp1 = self.gen_input1()
        root.wid1.layout.addWidget(root.wid1.inp1)
        root.wid1.setLayout(root.wid1.layout)
        root.layout.addWidget(root.wid1)
        root.wid2 = QWidget(root)
        root.wid2.layout = QHBoxLayout()
        root.wid2.layout.setAlignment(Qt.AlignLeft)
        root.wid2.label1 = QLabel(root.wid2)
        root.wid2.label1.setFont(self.bold)
        root.wid2.label1.setText(self.second_key)
        root.wid2.layout.addWidget(root.wid2.label1)
        root.wid2.inp1 = self.gen_input2()
        root.wid2.layout.addWidget(root.wid2.inp1)
        root.wid2.setLayout(root.wid2.layout)
        root.layout.addWidget(root.wid2)
        root.setLayout(root.layout)
        return root

    def set_text(self):
        self.first_key = "first key"
        self.second_key = "second key"

    def gen_input1(self):
        pass

    def gen_input2(self):
        pass

    def update(self):
        pass

class LogInput(BaseInput):
    def __init__(self,parent):
        super().__init__(parent,"log64.png")

    def set_text(self):
        self.first_key = "LogLevel : "
        self.second_key = "LogFile : "
        
    def gen_input1(self): #Dropdown menu for loglevel selection
        dd = QComboBox(self)
        dd.addItem("0 (Nothing)")
        dd.addItem("1 (Only critical information)")
        dd.addItem("2 (Key information)")
        dd.addItem("3 (Everything)")
        dd.setCurrentIndex(min(self.central.loglevel,3))
        dd.activated.connect(self.set_log)
        return dd

    def set_log(self):
        self.central.loglevel = self.textwidget.wid1.inp1.currentIndex()

    def gen_input2(self): #File brower for logfile
        b = QPushButton(self.central.logfile,self)
        b.clicked.connect(self.set_logfile)
        return b

    def set_logfile(self):
        lf = QFileDialog.getSaveFileName(self, 'Select log file', '.',filter=('Log files (*.log)'))[0]
        if lf:
            self.central.logfile = lf
            if len(lf) > 35:
                self.textwidget.wid2.inp1.setText("..."+lf[-31:])
            else:
                self.textwidget.wid2.inp1.setText(lf)

    def update(self):
        self.textwidget.wid1.inp1.setCurrentIndex(min(self.central.loglevel,3))
        lf = self.central.logfile
        if len(lf) > 35:
            self.textwidget.wid2.inp1.setText("..."+lf[-31:])
        else:
            self.textwidget.wid2.inp1.setText(lf)
    
class PlatInput(BaseInput):
    def __init__(self,parent):
        super().__init__(parent,"net64.png")

    def set_text(self):
        self.first_key = "Platform Host : "
        self.second_key = "Platform Port : "
        
    def gen_input1(self): #text input for host
        le = QLineEdit(self)
        le.setText(self.central.host)
        le.textChanged.connect(self.set_host)
        return le

    def set_host(self):
        self.central.host = self.textwidget.wid1.inp1.text()
    
    def gen_input2(self): #Number input for port
        sb = QSpinBox(self)
        sb.setMinimum(1)
        sb.setMaximum(999999)
        sb.setValue(self.central.port)
        sb.valueChanged.connect(self.set_port)
        return sb

    def set_port(self):
        self.central.port = self.textwidget.wid2.inp1.value()

    def update(self):
        self.textwidget.wid1.inp1.setText(self.central.host)
        self.textwidget.wid2.inp1.setValue(self.central.port)
