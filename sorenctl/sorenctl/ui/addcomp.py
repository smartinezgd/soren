#    ui/addcomp.py This file is part of SoREn
#    Copyright (C) 2019 Sébastien Martinez, Christophe Gransart (IFSTTAR)
#
#    This program is free software; you can redistribute it and/or modify
#    it under the terms of the GNU General Public License as published by
#    the Free Software Foundation; either version 2 of the License, or
#    (at your option) any later version.
#
#    This program is distributed in the hope that it will be useful,
#    but WITHOUT ANY WARRANTY; without even the implied warranty of
#    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#    GNU General Public License for more details.
#
#    You should have received a copy of the GNU General Public License along
#    with this program; if not, write to the Free Software Foundation, Inc.,
#    51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.

from PyQt5.QtWidgets import QMainWindow, QWidget, QHBoxLayout, QVBoxLayout, QLabel, QComboBox, QLineEdit, QPushButton, QFileDialog, QDialog, QTableWidget, QTableWidgetItem
from PyQt5.QtGui import QFont
from PyQt5.QtCore import Qt,pyqtSignal
from sorenctl.ui.widgets import gen_yes_no_dialog,gen_info_dialog
from sorenctl.ui.icons import get_icons_path
import os

icons = get_icons_path()

class AddCompWindow(QMainWindow):
    close_sig = pyqtSignal()
    
    def __init__(self,parent):
        super().__init__(parent)
        self.app = parent.app
        self.title = "Adding a component"
        self.left = 50
        self.top = 60
        self.width = 300
        self.height = 400
        self.setWindowTitle(self.title)
        self.setGeometry(self.left,self.top,self.width,self.height)
        self.central = AddCompCentral(self)
        self.setCentralWidget(self.central)

    def close_w(self):
        self.close_sig.emit()
        self.close()

    def edit(self,comp):
        self.central.edit(comp)

    def set_scenario(self,scenario):
        self.central.scenario = scenario
        
        
class AddCompCentral(QWidget):
    def __init__(self,parent):
        super().__init__(parent)
        self.app = parent.app
        self.win = parent
        self.path = ""
        self.name = ""
        self.ctype = "probe"
        self.parameters = {}
        self.scenario = None
        self.comp = None
        self.bold = QFont()
        self.bold.setBold(True)
        self.layout = QVBoxLayout()
        self.add_name_input()
        self.add_ctype_input()
        self.add_path_input()
        self.add_parameters_input()
        self.add_buttons()
        self.setLayout(self.layout)
        self.editting = False

    def edit(self,comp):
        self.editting = True
        self.comp = comp
        self.path = comp.path
        if len(self.path) > 30:
            self.path_input.selector.setText("..."+self.path[-27:])
        else:
            self.path_input.selector.setText(self.path)
        self.name = comp.name
        self.name_input.lineedit.setText(self.name)
        self.ctype = comp.ctype
        self.ctype_input.selector.setCurrentIndex(["probe","detector","decider","actuator"].index(comp.ctype))
        self.param_input.selector.clear()
        self.parameters = {}
        row = 0
        for p in comp.parameters.keys():
            value = comp.parameters[p]
            self.parameters[p] =  value
            self.param_input.selector.setItem(row,0,QTableWidgetItem(p))
            self.param_input.selector.setItem(row,1,QTableWidgetItem(str(value)))
            self.param_keys.append(p)
            row+=1

    def add_name_input(self):
        self.name_input = QWidget(self)
        self.name_input.layout = QHBoxLayout()
        self.name_input.label = QLabel(self)
        self.name_input.label.setFont(self.bold)
        self.name_input.label.setText("Name : ")
        self.name_input.layout.addWidget(self.name_input.label)
        self.name_input.lineedit = QLineEdit(self)
        self.name_input.lineedit.textChanged.connect(self.set_name)
        self.name_input.layout.addWidget(self.name_input.lineedit)
        self.name_input.setLayout(self.name_input.layout)
        self.layout.addWidget(self.name_input)

    def set_name(self):
        self.name = self.name_input.lineedit.text().strip()
    
    def add_ctype_input(self):
        self.ctype_input = QWidget(self)
        self.ctype_input.layout = QHBoxLayout()
        self.ctype_input.label = QLabel(self)
        self.ctype_input.label.setFont(self.bold)
        self.ctype_input.label.setText("Meta-Type : ")
        self.ctype_input.layout.addWidget(self.ctype_input.label)
        self.ctype_input.selector = QComboBox(self)
        self.ctype_input.selector.addItem("Probe")
        self.ctype_input.selector.addItem("Detector")
        self.ctype_input.selector.addItem("Decider")
        self.ctype_input.selector.addItem("Actuator")
        self.ctype_input.selector.activated.connect(self.set_ctype)
        self.ctype_input.setLayout(self.ctype_input.layout)
        self.ctype_input.layout.addWidget(self.ctype_input.selector)
        self.layout.addWidget(self.ctype_input)

    def set_ctype(self):
        self.ctype = self.ctype_input.selector.currentText().lower()

    def add_path_input(self):
        self.path_input = QWidget(self)
        self.path_input.layout = QHBoxLayout()
        self.path_input.label = QLabel(self)
        self.path_input.label.setFont(self.bold)
        self.path_input.label.setText("Path : ")
        self.path_input.layout.addWidget(self.path_input.label)
        self.path_input.selector = QPushButton("Select component path",self)
        self.path_input.selector.clicked.connect(self.set_path)
        self.path_input.layout.addWidget(self.path_input.selector)
        self.path_input.setLayout(self.path_input.layout)
        self.layout.addWidget(self.path_input)

    def set_path(self):
        path = QFileDialog.getExistingDirectory(self, 'Select path to component code', '.')
        if path:
            if len(path) > 30:
                self.path_input.selector.setText("..."+path[-27:])
            else:
                self.path_input.selector.setText(path)
            self.path = path

    def add_parameters_input(self):
        self.param_input = QWidget(self)
        self.param_input.layout = QVBoxLayout()
        self.param_input.label = QLabel(self)
        self.param_input.label.setFont(self.bold)
        self.param_input.label.setText("Parameters")
        self.param_input.layout.addWidget(self.param_input.label)
        self.param_input.selector = QTableWidget(self)
        self.param_input.selector.setColumnCount(2)
        self.param_input.selector.setRowCount(20)
        self.param_keys = [None]*20
        self.param_input.selector.setHorizontalHeaderItem(0,QTableWidgetItem("Key"))
        self.param_input.selector.setHorizontalHeaderItem(1,QTableWidgetItem("Value"))
        self.param_input.selector.cellChanged.connect(self.set_param)
        self.param_input.layout.addWidget(self.param_input.selector)
        self.param_input.setLayout(self.param_input.layout)
        self.layout.addWidget(self.param_input)

    def set_param(self,row,col):
        key = self.param_input.selector.item(row,0)
        skey = "_void"
        if key and key.text() != "":
            skey = key.text()
        k = self.param_keys[row]
        value = self.param_input.selector.item(row,1)
        if skey != "_void":
            if skey not in self.param_keys:
                self.param_keys[row] = skey                    
                if k != None and k in self.parameters.keys():
                    del self.parameters[k]
            if value != None:
                self.parameters[skey] = value.text().strip()        
        elif k != None:
            del self.parameters[k]
            self.param_keys[row] = None                
            
    def add_buttons(self):
        self.buttons = QWidget(self)
        self.buttons.layout = QHBoxLayout()
        self.buttons.cancel = QPushButton("Cancel",self.buttons)
        self.buttons.cancel.clicked.connect(self.cancel)
        self.buttons.layout.addWidget(self.buttons.cancel)
        self.buttons.ok = QPushButton("Validate",self.buttons)
        self.buttons.ok.clicked.connect(self.validate)
        self.buttons.layout.addWidget(self.buttons.ok)
        self.buttons.setLayout(self.buttons.layout)
        self.layout.addWidget(self.buttons)

    def clear(self):
        self.name = ""
        self.ctype = "probe"
        self.path = "Select component path"
        self.parameters = {}
        self.comp = None
        self.scenario = None
        self.name_input.lineedit.setText(self.name)
        self.ctype_input.selector.setCurrentIndex(0)
        self.path_input.selector.setText(self.path)
        self.param_input.selector.clear()
        self.editting = False
        self.param_keys = [None]*20

        
    def validate(self):
        #Do stuff
        if self.name == "":
            gen_info_dialog(self,"Error","No name specified for component!")
        elif self.ctype == "":
            gen_info_dialog(self,"Error","No meta type specified for component!")
        elif self.path == "":
            gen_info_dialog(self,"Error","No path specified for component!")
        else:
            if self.editting:
                self.comp.name = self.name
                self.comp.ctype = self.ctype
                self.comp.path = self.path
                if not self.comp.parse_manifest():
                    gen_info_dialog(self,"Warning!","Component has no manifest!")
                self.comp.parameters = self.parameters
            else:
                if self.app.has_component_named(self.name):
                    if gen_yes_no_dialog(self,"Warning","An element named "+self.name+" already exists\nOverwrite?"):
                        self.app.delete_component(self.app.get_component(self.name))
                    else:
                        return 
                if not self.scenario.add_component(self.ctype,self.name,self.path,self.parameters):
                    gen_info_dialog(self,"Warning!","Component has no manifest!")

            self.clear()
            self.win.close_w()

    def cancel(self):
        self.clear()
        self.win.close_w()

